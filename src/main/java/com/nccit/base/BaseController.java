package com.nccit.base;

import com.nccit.ceu.model.ResultInfo;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;

/**
 * 基础控制层
 */
public class BaseController {

    /**
     * 返回成功信息
     *
     * @param msg
     * @return
     */
    public ResultInfo success(String msg) {
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setMsg(msg);
        return resultInfo;
    }

    /**
     * 返回成功信息和结果集
     *
     * @param msg
     * @param result
     * @return
     */
    public ResultInfo success(String msg, Object result) {
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setMsg(msg);
        resultInfo.setResult(result);
        return resultInfo;
    }

    /**
     * 请求中加入"ctx"变量
     *
     * @param request
     */
    @ModelAttribute
    public void preMethod(HttpServletRequest request) {
        request.setAttribute("ctx", request.getContextPath());
    }
}
