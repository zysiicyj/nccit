package com.nccit.ceu.po;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户角色POJO
 */
public class UserRole implements Serializable {
    private static final long serialVersionUID = -5325867045610849103L;
    private Integer id;
    //用户ID
    private Integer userId;
    //角色ID
    private Integer roleId;
    //创建时间
    private Date createDate;
    //更新时间
    private Date updateDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}