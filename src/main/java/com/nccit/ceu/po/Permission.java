package com.nccit.ceu.po;

import java.io.Serializable;
import java.util.Date;

/**
 * 权限POJO
 */
public class Permission implements Serializable {
    private static final long serialVersionUID = -6632048563877666180L;
    private Integer id;
    //角色ID
    private Integer roleId;
    //模块ID
    private Integer moduleId;
    //操作值
    private String aclValue;
    //创建时间
    private Date createDate;
    //更新时间
    private Date updateDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public String getAclValue() {
        return aclValue;
    }

    public void setAclValue(String aclValue) {
        this.aclValue = aclValue == null ? null : aclValue.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}