package com.nccit.ceu.po;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 竞赛POJO
 */
public class Competition implements Serializable {

    private static final long serialVersionUID = -2720365364195577780L;
    private Integer id;
    //竞赛名称
    private String coName;
    //官网
    private String coAddress;
    //赛事介绍
    private String coIntroduce;
    //是否有效
    private Integer isValid;
    //创建人
    private String createMan;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss", timezone = "GMT+8")
    private Date createDate;
    //更新时间
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss", timezone = "GMT+8")
    private Date updateDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCoName() {
        return coName;
    }

    public void setCoName(String coName) {
        this.coName = coName == null ? null : coName.trim();
    }

    public String getCoAddress() {
        return coAddress;
    }

    public void setCoAddress(String coAddress) {
        this.coAddress = coAddress == null ? null : coAddress.trim();
    }

    public String getCoIntroduce() {
        return coIntroduce;
    }

    public void setCoIntroduce(String coIntroduce) {
        this.coIntroduce = coIntroduce == null ? null : coIntroduce.trim();
    }

    public Integer getIsValid() {
        return isValid;
    }

    public void setIsValid(Integer isValid) {
        this.isValid = isValid;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan == null ? null : createMan.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}