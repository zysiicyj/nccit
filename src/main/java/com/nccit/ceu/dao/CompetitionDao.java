package com.nccit.ceu.dao;

import com.nccit.base.BaseDao;
import com.nccit.ceu.po.Competition;

/**
 * 竞赛信息DAO
 */
public interface CompetitionDao extends BaseDao<Competition> {
}