package com.nccit.ceu.dao;

import com.nccit.base.BaseDao;
import com.nccit.ceu.po.FileSharing;

/**
 * 文件管理DAO
 */
public interface FileSharingDao extends BaseDao<FileSharing> {
    int deleteByPrimaryKey(Integer[] ids);

    int insert(FileSharing record);

    int insertSelective(FileSharing record);

    FileSharing selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(FileSharing record);
}