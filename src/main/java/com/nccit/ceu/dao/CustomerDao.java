package com.nccit.ceu.dao;

import com.nccit.base.BaseDao;
import com.nccit.ceu.po.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户管理DAO
 */
public interface CustomerDao extends BaseDao<Customer> {

    public Customer queryCustomerByCusName(@Param("cusName") String cusName);

    public List<Customer> queryLossCustomers();
}