package com.nccit.ceu.dao;

import com.nccit.base.BaseDao;
import com.nccit.ceu.po.News;
import org.apache.ibatis.annotations.Param;

/**
 * 新闻管理DAO
 */
public interface NewsDao extends BaseDao<News> {
    public Integer updateNewsDevResultBySid(@Param("sid") Integer sid, @Param("devResult") Integer devResult);
}
