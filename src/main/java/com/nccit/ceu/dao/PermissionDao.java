package com.nccit.ceu.dao;

import com.nccit.base.BaseDao;
import com.nccit.ceu.po.Permission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 权限管理DAO
 */
public interface PermissionDao extends BaseDao<Permission> {

    public Integer queryPermissionTotalByRoleId(@Param("roleId") Integer roleId);

    public Integer deletePermissionByRoleId(@Param("roleId") Integer roleId);

    public List<Integer> queryModuleIdsByRoleId(@Param("roleId") Integer roleId);
}