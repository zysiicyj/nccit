package com.nccit.ceu.dao;

import com.nccit.base.BaseDao;
import com.nccit.ceu.dto.UserDto;
import com.nccit.ceu.po.User;
import com.nccit.ceu.query.UserQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 用户管理DAO
 */
public interface UserDao extends BaseDao<User> {
    public User queryUserByUserName(@Param("userName") String userName);

    public Integer updateUserPasswordById(@Param("userId") Integer userId, @Param("userPwd") String userPwd);

    public List<Map<String, Object>> queryCustomerManagers();

    public List<UserDto> queryUsersByParams(UserQuery userQuery);

    public List<Integer> queryAllRoleIdsByUserId(@Param("userId") Integer userId);
}

