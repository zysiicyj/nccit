package com.nccit.ceu.dao;

import com.nccit.base.BaseDao;
import com.nccit.ceu.po.Role;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 角色管理DAO
 */
public interface RoleDao extends BaseDao<Role> {

    public List<Role> queryAllRoles();

    @Select("select id,role_name as roleName,role_remark as roleRemark ,create_date as createDate," +
            "update_date as updateDate from t_role where role_name=#{roleName} and is_valid=1")
    public Role queryRoleByRoleName(@Param("roleName") String roleName);

    public List<String> queryAllPermissionsByRoleIds(List<Integer> roleIds);
}