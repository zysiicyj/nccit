package com.nccit.ceu.dao;

import com.nccit.base.BaseDao;
import com.nccit.ceu.po.NewsDevPlan;

/**
 * 新闻开发管理DAO
 */
public interface NewsDevPlanDao extends BaseDao<NewsDevPlan> {

}