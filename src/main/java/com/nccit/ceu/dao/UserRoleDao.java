package com.nccit.ceu.dao;

import com.nccit.base.BaseDao;
import com.nccit.ceu.po.UserRole;

/**
 * 用户角色DAO
 */
public interface UserRoleDao extends BaseDao<UserRole> {

    public Integer queryUserRoleTotalByUserId(Integer userId);

    public Integer deleteUserRoleByUserId(Integer userId);

    public Integer queryUserRoleTotalByRoleId(Integer roleId);

    public Integer deleteUserRoleByRoleId(Integer roleId);
}