package com.nccit.ceu.model;

import com.nccit.ceu.constants.CeuConstant;

/**
 * 结果集
 */
public class ResultInfo {
    private Integer code = CeuConstant.OPS_SUCCESS_CODE;
    private String msg = CeuConstant.OPS_SUCCESS_MSG;
    private Object result;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
