package com.nccit.ceu.exceptions;

import com.alibaba.fastjson.JSON;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.model.ResultInfo;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

/**
 * 全局异常处理
 */
@Component
public class GlobalExceptionResolver implements HandlerExceptionResolver {
    /**
     * 重写
     * 1、判断是否是AuthException的实例
     * 2、处理全局异常
     *
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @return
     */
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        ModelAndView mv = getDefaultModelAndView(request, ex);

        if (ex instanceof AuthException) {
            AuthException ae = (AuthException) ex;
            mv.setViewName("au_error");
            mv.addObject("errorMsg", ae.getMsg());
            mv.addObject("errorCode", ae.getCode());
            return mv;
        }

        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            ResponseBody responseBody = method.getAnnotation(ResponseBody.class);
            if (null != responseBody) {
                ResultInfo resultInfo = new ResultInfo();
                resultInfo.setCode(CeuConstant.OPS_FAILED_CODE);
                resultInfo.setMsg(CeuConstant.OPS_FAILED_MSG);
                if (ex instanceof ParamsException) {
                    ParamsException pe = (ParamsException) ex;
                    resultInfo.setCode(pe.getCode());
                    resultInfo.setMsg(pe.getMsg());
                }
                response.setCharacterEncoding("utf-8");
                response.setContentType("application/json;charset=utf-8");
                PrintWriter pw = null;
                try {
                    pw = response.getWriter();
                    pw.write(JSON.toJSONString(resultInfo));
                    pw.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (null != pw) {
                        pw.close();
                    }
                }
                return null;
            } else {
                if (ex instanceof ParamsException) {
                    ParamsException pe = (ParamsException) ex;
                    mv.addObject("errorMsg", pe.getMsg());
                    mv.addObject("errorCode", pe.getCode());
                }
                return mv;
            }
        } else {
            return mv;
        }
    }

    /**
     * 获取默认视图
     *
     * @param request
     * @param ex
     * @return
     */
    private ModelAndView getDefaultModelAndView(HttpServletRequest request, Exception ex) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("error");
        mv.addObject("ctx", request.getContextPath());
        mv.addObject("errorMsg", CeuConstant.OPS_FAILED_MSG);
        mv.addObject("errorCode", CeuConstant.OPS_FAILED_CODE);
        mv.addObject("uri", request.getRequestURI());
        return mv;
    }
}
