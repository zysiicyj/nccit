package com.nccit.ceu.exceptions;

import com.nccit.ceu.constants.CeuConstant;

/**
 * 参数异常
 */
public class ParamsException extends RuntimeException {
    private Integer code = CeuConstant.OPS_FAILED_CODE;
    private String msg = "操作失败!";

    /**
     * 根据状态码、异常信息
     *
     * @param code
     * @param msg
     */
    public ParamsException(Integer code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    /**
     * 根据状态码
     *
     * @param code
     */
    public ParamsException(Integer code) {
        super("操作失败");
        this.code = code;
    }

    /**
     * 根据异常信息
     *
     * @param msg
     */
    public ParamsException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
