package com.nccit.ceu.exceptions;

import com.nccit.ceu.constants.CeuConstant;

/**
 *
 */
public class AuthException extends RuntimeException {
    private Integer code = CeuConstant.USER_NOT_LOGIN_CODE;
    private String msg = CeuConstant.USER_NOT_LOGIN_MSG;

    public AuthException(Integer code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    public AuthException(Integer code) {
        super("操作失败");
        this.code = code;
    }

    public AuthException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
