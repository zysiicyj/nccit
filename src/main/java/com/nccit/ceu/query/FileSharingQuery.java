package com.nccit.ceu.query;

import com.nccit.base.BaseQuery;

import java.util.Date;

/**
 * 文件查询
 */
public class FileSharingQuery extends BaseQuery {
    private String name;
    private Integer type;
    private String author;
    private String time;

    @Override
    public String toString() {
        return "FileSharingQuery{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", author='" + author + '\'' +
                ", updateTime=" + time +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
