package com.nccit.ceu.query;

import com.nccit.base.BaseQuery;

/**
 * 新闻计划查询
 */
public class NewsDevPlanQuery extends BaseQuery {
    private Integer sid;

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }
}
