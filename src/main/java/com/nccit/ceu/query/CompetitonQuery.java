package com.nccit.ceu.query;

import com.nccit.base.BaseQuery;

import java.util.Date;

/**
 * 竞赛查询
 */
public class CompetitonQuery extends BaseQuery {
    private String coName;
    private String coAddress;
    private String time;

    public String getCoName() {
        return coName;
    }

    public void setCoName(String coName) {
        this.coName = coName;
    }

    public String getCoAddress() {
        return coAddress;
    }

    public void setCoAddress(String coAddress) {
        this.coAddress = coAddress;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
