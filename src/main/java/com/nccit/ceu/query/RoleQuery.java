package com.nccit.ceu.query;

import com.nccit.base.BaseQuery;

/**
 * 角色查询
 */
public class RoleQuery extends BaseQuery {
    private String roleName;
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRoleName() {

        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
