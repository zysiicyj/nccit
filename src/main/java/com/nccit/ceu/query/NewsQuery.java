package com.nccit.ceu.query;

import com.nccit.base.BaseQuery;

/**
 * 新闻查询
 */
public class NewsQuery extends BaseQuery {
    private String newsName;//新闻名称
    private Integer state;// 发布状态
    private Integer devResult;//开发结果
    private String time;//创建时间
    private String newsTitle;//新闻标题

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsName() {
        return newsName;
    }

    public void setNewsName(String newsName) {
        this.newsName = newsName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getDevResult() {
        return devResult;
    }

    public void setDevResult(Integer devResult) {
        this.devResult = devResult;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
