package com.nccit.ceu.query;

import com.nccit.base.BaseQuery;

/**
 * 用户查询
 */
public class CustomerQuery extends BaseQuery {
    private String cusName;


    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }


}
