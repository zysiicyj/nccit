package com.nccit.ceu.controller;

import com.nccit.base.BaseController;
import com.nccit.ceu.po.User;
import com.nccit.ceu.service.UserService;
import com.nccit.ceu.utils.LoginUserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 首页管理
 */
@Controller
public class MainController extends BaseController {

    @Resource
    private UserService userService;

    /**
     * 根据用户id获取用户信息
     *
     * @param request
     * @return
     */
    @RequestMapping("main")
    public String main(HttpServletRequest request) {
        Integer userId = LoginUserUtil.releaseUserIdFromCookie(request);
        User user = userService.queryById(userId);
        request.setAttribute("user", user);
        return "main";
    }
}
