package com.nccit.ceu.controller;

import com.nccit.base.BaseController;
import com.nccit.ceu.annotations.RequestPermission;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.model.ResultInfo;
import com.nccit.ceu.po.Customer;
import com.nccit.ceu.query.CustomerQuery;
import com.nccit.ceu.service.CustomerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 用户管理
 */
@Controller
@RequestMapping("customer")
public class CustomerController extends BaseController {

    @Resource
    private CustomerService customerService;

    /**
     * 页面跳转
     *
     * @return
     */
    @RequestMapping("index")
    public String index() {
        return "customer";
    }

    /**
     * 查询返回用户信息
     *
     * @param customerQuery
     * @param page
     * @param rows
     * @return
     */
    @RequestPermission(aclValue = "201001")
    @RequestMapping("queryCustomersByParams")
    @ResponseBody
    public Map<String, Object> queryCustomersByParams(CustomerQuery customerQuery, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer rows) {
        customerQuery.setPageNum(page);
        customerQuery.setPageSize(rows);
        return customerService.queryForPage(customerQuery);
    }

    /**
     * 添加或者更新用户信息
     *
     * @param customer
     * @return
     */
    @RequestPermission(aclValue = "201002")
    @RequestMapping("saveOrUpdateCustomer")
    @ResponseBody
    public ResultInfo saveOrUpdateCustomer(Customer customer) {
        customerService.saveOrUpdateCustomer(customer);
        return success(CeuConstant.OPS_SUCCESS_MSG);
    }

    /**
     * 批量删除用户信息
     *
     * @param ids
     * @return
     */
    @RequestPermission(aclValue = "201003")
    @RequestMapping("deleteCustomersByIds")
    @ResponseBody
    public ResultInfo deleteCustomersByIds(Integer[] ids) {
        customerService.deleteCustomersByIds(ids);
        return success(CeuConstant.OPS_SUCCESS_MSG);
    }
}
