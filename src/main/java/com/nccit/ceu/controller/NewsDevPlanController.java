package com.nccit.ceu.controller;

import com.nccit.base.BaseController;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.model.ResultInfo;
import com.nccit.ceu.po.NewsDevPlan;
import com.nccit.ceu.po.News;
import com.nccit.ceu.query.NewsDevPlanQuery;
import com.nccit.ceu.service.NewsDevPlanService;
import com.nccit.ceu.service.NewsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 新闻开发管理
 */
@Controller
@RequestMapping("newsDevPlan")
public class NewsDevPlanController extends BaseController {

    @Resource
    private NewsService newsService;

    @Resource
    private NewsDevPlanService newsDevPlanService;

    /**
     * 页面控制
     *
     * @param sid
     * @param model
     * @return
     */
    @RequestMapping("index")
    public String index(Integer sid, Model model) {
        News news = newsService.queryById(sid);
        model.addAttribute("news", news);
        return "news_dev_plan_detail";
    }

    /**
     * 根据参数参训新闻开发信息
     *
     * @param page
     * @param rows
     * @param newsDevPlanQuery
     * @return
     */
    @RequestMapping("queryNewsDevPlansBySid")
    @ResponseBody
    public Map<String, Object> queryNewsDevPlansBySid(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer rows, NewsDevPlanQuery newsDevPlanQuery) {
        newsDevPlanQuery.setPageNum(page);
        newsDevPlanQuery.setPageSize(rows);
        return newsDevPlanService.queryForPage(newsDevPlanQuery);
    }

    /**
     * 添加或者更新新闻信息
     *
     * @param newsDevPlan
     * @param sid
     * @return
     */
    @RequestMapping("saveOrUpdateNewsDevPlan")
    @ResponseBody
    public ResultInfo saveOrUpdateNewsDevPlan(NewsDevPlan newsDevPlan, Integer sid) {
        newsDevPlan.setNewsId(sid);
        newsDevPlanService.saveOrUpdateNewsDevPlan(newsDevPlan);
        return success(CeuConstant.OPS_SUCCESS_MSG);
    }

    /**
     * 批量删除新闻信息
     *
     * @param ids
     * @return
     */
    @RequestMapping("delNewsDevPlan")
    @ResponseBody
    public ResultInfo delNewsDevPlan(Integer[] ids) {
        newsDevPlanService.deleteNewsDevPlanBatch(ids);
        return success(CeuConstant.OPS_SUCCESS_MSG);
    }
}