package com.nccit.ceu.controller;

import com.nccit.base.BaseController;
import com.nccit.ceu.annotations.RequestPermission;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.model.ResultInfo;
import com.nccit.ceu.po.News;
import com.nccit.ceu.po.User;
import com.nccit.ceu.query.NewsQuery;
import com.nccit.ceu.service.NewsService;
import com.nccit.ceu.service.UserService;
import com.nccit.ceu.utils.LoginUserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 新闻管理
 */
@Controller
@RequestMapping("news")
public class NewsController extends BaseController {
    @Resource
    private NewsService newsService;

    @Resource
    private UserService userService;

    /**
     * 页面转发
     *
     * @param state
     * @return
     */
    @RequestMapping("index/{state}")
    public String index(@PathVariable Integer state) {
        if (null != state) {
            if (state == 0) {
                return "news";
            } else if (state == 1) {
                return "news_dev_plan";
            } else {
                return "error";
            }
        } else {
            return "error";
        }
    }

    /**
     * 根据参数查询新闻信息
     *
     * @param page
     * @param rows
     * @param newsQuery
     * @return
     */
    @RequestPermission(aclValue = "101001")
    @RequestMapping("queryNewsByParams")
    @ResponseBody
    public Map<String, Object> queryNewsByParams(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer rows, NewsQuery newsQuery) {
        newsQuery.setPageNum(page);
        newsQuery.setPageSize(rows);
        return newsService.queryForPage(newsQuery);
    }

    /**
     * 添加或者更新新闻信息
     *
     * @param request
     * @param news
     * @return
     */
    @RequestPermission(aclValue = "101002")
    @RequestMapping("saveOrUpdateNews")
    @ResponseBody
    public ResultInfo saveOrUpdateNews(HttpServletRequest request, News news) {
        Integer userId = LoginUserUtil.releaseUserIdFromCookie(request);
        User user = userService.queryById(userId);
        news.setCreateMan(user.getTrueName());
        newsService.saveOrUpdateNews(news);
        return success(CeuConstant.OPS_SUCCESS_MSG);
    }

    /**
     * 批量删除新闻
     *
     * @param ids
     * @return
     */
    @RequestPermission(aclValue = "101003")
    @RequestMapping("deleteNewsBatch")
    @ResponseBody
    public ResultInfo deleteNewsBatch(Integer[] ids) {
        newsService.deleteNewsBatch(ids);
        return success("记录删除成功!");
    }

    /**
     * 根据ID更新新闻开发状态
     *
     * @param sid
     * @param devResult
     * @return
     */
    @RequestMapping("updateNewsDevResultBySid")
    @ResponseBody
    public ResultInfo updateNewsDevResultBySid(Integer sid, Integer devResult) {
        newsService.updateNewsDevResultBySid(sid, devResult);
        return success("状态更新成功!");
    }
}
