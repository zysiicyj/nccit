package com.nccit.ceu.controller;

import com.nccit.base.BaseController;
import com.nccit.ceu.annotations.RequestPermission;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.model.ResultInfo;
import com.nccit.ceu.po.FileSharing;
import com.nccit.ceu.po.User;
import com.nccit.ceu.query.FileSharingQuery;
import com.nccit.ceu.service.FileSharingService;
import com.nccit.ceu.service.UserService;
import com.nccit.ceu.utils.LoginUserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 文件管理
 */
@Controller
@RequestMapping("fileSharing")
public class FileSharingController extends BaseController {
    @Resource
    private FileSharingService fileSharingService;

    @Resource
    private UserService userService;

    /**
     * 页面跳转
     *
     * @param state
     * @return
     */
    @RequestMapping("index/{state}")
    public String index(@PathVariable Integer state) {
        if (null != state) {
            if (state == 0) {
                return "fileSharing";
            } else if (state == 1) {
                return "fileSharing_dev_plan";
            } else {
                return "error";
            }
        } else {
            return "error";
        }
    }

    /**
     * 根据参数查询文件信息
     *
     * @param page
     * @param rows
     * @param fileSharingQuery
     * @return
     */
    @RequestPermission(aclValue = "401001")
    @RequestMapping("queryFileSharingByParams")
    @ResponseBody
    public Map<String, Object> queryFileSharingByParams(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer rows, FileSharingQuery fileSharingQuery) {
        fileSharingQuery.setPageNum(page);
        fileSharingQuery.setPageSize(rows);
        return fileSharingService.queryForPage(fileSharingQuery);
    }

    /**
     * 添加或者更新文件
     *
     * @param request
     * @param fileSharing
     * @return
     */
    @RequestPermission(aclValue = "401002")
    @RequestMapping("saveOrUpdateFileSharing")
    @ResponseBody
    public ResultInfo saveOrUpdateFileSharing(HttpServletRequest request, FileSharing fileSharing) {
        fileSharingService.saveOrUpdateFileSharing(fileSharing);
        return success(CeuConstant.OPS_SUCCESS_MSG);
    }

    /**
     * 批量删除文件
     *
     * @param ids
     * @return
     */
    @RequestPermission(aclValue = "401003")
    @RequestMapping("deleteFileSharingBatch")
    @ResponseBody
    public ResultInfo deleteFileSharingBatch(Integer[] ids) {
        fileSharingService.deleteFileSharingBatch(ids);
        return success("记录删除成功!");
    }
}
