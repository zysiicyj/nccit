package com.nccit.ceu.controller;

import com.nccit.base.BaseController;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.dto.UserDto;
import com.nccit.ceu.exceptions.ParamsException;
import com.nccit.ceu.model.ResultInfo;
import com.nccit.ceu.model.UserModel;
import com.nccit.ceu.po.User;
import com.nccit.ceu.query.UserQuery;
import com.nccit.ceu.service.UserService;
import com.nccit.ceu.utils.LoginUserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.transform.Result;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 用户管理
 */
@Controller
@RequestMapping("user")
public class UserController extends BaseController {

    @Resource
    private UserService userService;

    /**
     * 页面控制
     *
     * @return
     */
    @RequestMapping("index")
    public String index() {
        return "user";
    }

    /**
     * 用户登录
     *
     * @param userName
     * @param userPwd
     * @param session
     * @return
     */
    @RequestMapping("userLogin")
    @ResponseBody
    public ResultInfo userLogin(String userName, String userPwd, HttpSession session) {
        UserModel userModel = userService.userLoginCheck(userName, userPwd);
        List<String> permissions = userService.queryUserHasPermissions(userName);
        session.setAttribute(CeuConstant.USER_PERMISSIONS, permissions);//将权限值放入session
        return success("用户登录成功", userModel);
    }

    /**
     * 更新密码
     *
     * @param oldPassword
     * @param newPassword
     * @param confirmPassword
     * @param request
     * @return
     */
    @RequestMapping("updateUserPassword")
    @ResponseBody
    public ResultInfo updateUserPassword(String oldPassword, String newPassword, String confirmPassword, HttpServletRequest request) {
        Integer userId = LoginUserUtil.releaseUserIdFromCookie(request);
        userService.updateUserPasswordByUserId(userId, oldPassword, newPassword, confirmPassword);
        return success("密码更新成功!");
    }

    /**
     * 查询真实姓名
     *
     * @return
     */
    @RequestMapping("queryCustomerManagers")
    @ResponseBody
    public List<Map<String, Object>> queryCustomerManagers() {
        return userService.queryCustomerManagers();
    }

    /**
     * 根据参数查询用户信息
     *
     * @param userQuery
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("queryUsersByParams")
    @ResponseBody
    public Map<String, Object> queryUsersByParams(UserQuery userQuery, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer rows) {
        userQuery.setPageNum(page);
        userQuery.setPageSize(rows);
        return userService.queryUsersByParams(userQuery);
    }

    /**
     * 添加或者更新用户信息
     *
     * @param userDto
     * @return
     */
    @RequestMapping("saveOrUpdateUser")
    @ResponseBody
    public ResultInfo saveOrUpdateUser(UserDto userDto) {
        List<Integer> roleIds = userDto.getRoleIds();
        List<Integer> target = null;
        if (!CollectionUtils.isEmpty(roleIds)) {
            target = new ArrayList<Integer>();
            for (Integer roleId : roleIds) {
                if (null != roleId) {
                    target.add(roleId);
                }
            }
            userDto.setRoleIds(target);
        }
        userService.saveOrUpdateUser(userDto);
        return success(CeuConstant.OPS_SUCCESS_MSG);
    }

    /**
     * 批量删除用户信息
     *
     * @param ids
     * @return
     */
    @RequestMapping("deleteUserBatch")
    @ResponseBody
    public ResultInfo deleteUserBatch(Integer[] ids) {
        userService.deleteUserBatch(ids);
        return success(CeuConstant.OPS_SUCCESS_MSG);
    }
}
