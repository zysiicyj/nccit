package com.nccit.ceu.controller;

import com.nccit.base.BaseController;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.model.ResultInfo;
import com.nccit.ceu.po.Role;
import com.nccit.ceu.query.RoleQuery;
import com.nccit.ceu.service.RoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.xml.transform.Result;
import java.util.List;
import java.util.Map;

/**
 * 角色管理
 */
@Controller
@RequestMapping("role")
public class RoleController extends BaseController {
    @Resource
    private RoleService roleService;

    /**
     * 页面控制
     *
     * @return
     */
    @RequestMapping("index")
    public String index() {
        return "role";
    }

    /**
     * 查询所有角色
     *
     * @return
     */
    @RequestMapping("queryAllRoles")
    @ResponseBody
    public List<Role> queryAllRoles() {
        return roleService.queryAllRoles();
    }

    /**
     * 根据参数查询角色信息
     *
     * @param roleQuery
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("queryRolesByParams")
    @ResponseBody
    public Map<String, Object> queryRolesByParams(RoleQuery roleQuery, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer rows) {
        roleQuery.setPageNum(page);
        roleQuery.setPageSize(rows);
        return roleService.queryForPage(roleQuery);
    }

    /**
     * 添加或者更新角色信息
     *
     * @param role
     * @return
     */
    @RequestMapping("saveOrUpdateRole")
    @ResponseBody
    public ResultInfo saveOrUpdateRole(Role role) {
        roleService.saveOrUpdateRole(role);
        return success(CeuConstant.OPS_SUCCESS_MSG);
    }

    /**
     * 批量删除角色信息
     *
     * @param ids
     * @return
     */
    @RequestMapping("deleteRolesBatch")
    @ResponseBody
    public ResultInfo deleteRolesBatch(Integer[] ids) {
        roleService.deleteRolesBatch(ids);
        return success(CeuConstant.OPS_SUCCESS_MSG);
    }

    /**
     * 添加等级（授权）
     *
     * @param rid
     * @param moduleIds
     * @return
     */
    @RequestMapping("addGrant")
    @ResponseBody
    public ResultInfo addGrant(Integer rid, Integer[] moduleIds) {
        roleService.addGrant(rid, moduleIds);
        return success("角色授权成功");
    }
}

