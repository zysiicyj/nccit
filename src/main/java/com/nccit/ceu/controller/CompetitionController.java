package com.nccit.ceu.controller;

import com.nccit.base.BaseController;
import com.nccit.ceu.annotations.RequestPermission;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.model.ResultInfo;
import com.nccit.ceu.po.Competition;
import com.nccit.ceu.query.CompetitonQuery;
import com.nccit.ceu.service.CompetitionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 竞赛管理
 */
@RequestMapping("competition")
@Controller
public class CompetitionController extends BaseController {

    @Resource
    private CompetitionService competitionService;

    /**
     * 页面跳转
     *
     * @param state
     * @return
     */
    @RequestMapping("index/{state}")
    public String index(@PathVariable Integer state) {
        if (null != state) {
            if (state == 0) {
                return "competiton";
            } else if (state == 1) {
                return "competiton_dev_plan";
            } else {
                return "error";
            }
        } else {
            return "error";
        }
    }

    /**
     * 根据参数查询竞赛信息
     *
     * @param page
     * @param rows
     * @param competitonQuery
     * @return
     */
    @RequestPermission(aclValue = "301001")
    @RequestMapping("queryCompetitionByParams")
    @ResponseBody
    public Map<String, Object> queryCompetitionByParams(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer rows, CompetitonQuery competitonQuery) {
        competitonQuery.setPageNum(page);
        competitonQuery.setPageSize(rows);
        return competitionService.queryForPage(competitonQuery);
    }

    /**
     * 添加或更新竞赛信息
     *
     * @param request
     * @param competition
     * @return
     */
    @RequestPermission(aclValue = "301002")
    @RequestMapping("saveOrUpdateCompetition")
    @ResponseBody
    public ResultInfo saveOrUpdateCompetition(HttpServletRequest request, Competition competition) {
        competition.setIsValid(1);
        competitionService.saveOrUpdateCompetiton(competition);
        return success(CeuConstant.OPS_SUCCESS_MSG);
    }

    /**
     * 批量删除竞赛信息
     *
     * @param ids
     * @return
     */
    @RequestPermission(aclValue = "301003")
    @RequestMapping("deleteCompetitionBatch")
    @ResponseBody
    public ResultInfo deleteCompetitionBatch(Integer[] ids) {
        competitionService.deleteCompetitonBatch(ids);
        return success("记录删除成功!");
    }
}
