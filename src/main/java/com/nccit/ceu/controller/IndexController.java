package com.nccit.ceu.controller;

import com.nccit.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 主页管理
 */
@Controller
public class IndexController extends BaseController {

    /**
     * 页面跳转
     *
     * @return
     */
    @RequestMapping("index")
    public String index() {
        return "index";
    }
}
