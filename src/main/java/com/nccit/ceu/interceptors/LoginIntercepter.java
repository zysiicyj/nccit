package com.nccit.ceu.interceptors;

import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.service.UserService;
import com.nccit.ceu.utils.AssertUtil;
import com.nccit.ceu.utils.LoginUserUtil;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录过滤控制
 */
public class LoginIntercepter extends HandlerInterceptorAdapter {

    @Resource
    private UserService userService;

    /**
     * 请求之前处理
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Integer userId = LoginUserUtil.releaseUserIdFromCookie(request);
        AssertUtil.isNotLogin(null == userId || null == userService.queryById(userId), CeuConstant.USER_NOT_LOGIN_MSG);
        return true;
    }
}
