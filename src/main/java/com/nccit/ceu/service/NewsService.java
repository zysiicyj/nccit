package com.nccit.ceu.service;

import com.nccit.base.BaseService;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.dao.NewsDao;
import com.nccit.ceu.po.News;
import com.nccit.ceu.utils.AssertUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 新闻服务
 */
@Service
public class NewsService extends BaseService<News> {

    @Resource
    private NewsDao newsDao;

    /**
     * 1、参数校验
     * 2、指派人
     * 有：已发布
     * 无：未发布
     * 3、ID
     * 有：更新
     * 无：添加
     *
     * @param news
     */
    public void saveOrUpdateNews(News news) {
        checkNewsParams(news.getNewsName(), news.getNewsAuthor(), news.getNewsTitle());
        news.setUpdateDate(new Date());
        if (!StringUtils.isBlank(news.getAssignMan())) {
            news.setAssignTime(new Date());
            news.setState(1);
        } else {
            news.setState(0);
        }
        if (null == news.getId()) {
            news.setCreateDate(new Date());
            news.setDevResult(0);
            news.setIsValid(1);
            AssertUtil.isTrue(newsDao.save(news) < 1, CeuConstant.OPS_FAILED_MSG);
        } else {
            news.setUpdateDate(new Date());
            if (!StringUtils.isBlank(news.getAssignMan())) {
                news.setAssignTime(new Date());
                news.setState(1);
            } else {
                news.setState(0);
            }
            if (null == news.getId()) {
                news.setCreateDate(new Date());
                news.setDevResult(0);
                news.setIsValid(1);
                AssertUtil.isTrue(newsDao.save(news) < 1, CeuConstant.OPS_FAILED_MSG);
            } else {
                AssertUtil.isTrue(null == newsDao.queryById(news.getId()), "待更新记录不存在或已删除!");
                AssertUtil.isTrue(newsDao.update(news) < 1, CeuConstant.OPS_FAILED_MSG);
            }

        }
    }

    /**
     * 参数校验
     * 1、新闻名称不能为空
     * 2、新闻作者不能为空
     * 3、新闻标题不能为空
     *
     * @param newsName
     * @param newsAuthor
     */
    private void checkNewsParams(String newsName, String newsAuthor, String newsTitle) {
        AssertUtil.isTrue(StringUtils.isBlank(newsName), "新闻名称不能为空!");
        AssertUtil.isTrue(StringUtils.isBlank(newsAuthor), "请提供作者!");
        AssertUtil.isTrue(StringUtils.isBlank(newsTitle), "新闻标题不能为空！");
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public void deleteNewsBatch(Integer[] ids) {
        AssertUtil.isTrue(null == ids || ids.length == 0, "请选择待删除记录!");
        AssertUtil.isTrue(newsDao.deleteBatch(ids) < ids.length, CeuConstant.OPS_FAILED_MSG);
    }

    /**
     * 根据ID更新新闻开发结果
     *
     * @param sid
     * @param devResult
     */
    public void updateNewsDevResultBySid(Integer sid, Integer devResult) {

        AssertUtil.isTrue(null == sid || null == newsDao.queryById(sid), "待更新新闻数据不存在!");

        AssertUtil.isTrue(null == sid || null == newsDao.queryById(sid), "待更新新闻机会数据不存在!");

        AssertUtil.isTrue(newsDao.updateNewsDevResultBySid(sid, devResult) < 1, CeuConstant.OPS_FAILED_MSG);
    }
}
