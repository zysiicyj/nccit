package com.nccit.ceu.service;

import com.nccit.base.BaseService;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.dao.CompetitionDao;
import com.nccit.ceu.po.Competition;
import com.nccit.ceu.utils.AssertUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 竞赛信息服务
 */
@Service
public class CompetitionService extends BaseService<Competition> {
    @Resource
    private CompetitionDao competitonDao;

    /**
     * 1.参数校验
     * 参数非空校验
     * competiton_name
     * competiton_author
     * link_phone
     * 2.判断添加|更新操作 唯一标准-id值是否存在
     * id 存在:执行更新
     * id 不存在:执行添加
     * 3.判断结果
     *
     * @param competiton
     */
    public void saveOrUpdateCompetiton(Competition competiton) {
        checkCompetitonParams(competiton.getCoName(), competiton.getCreateMan());
        competiton.setUpdateDate(new Date());
        if (null == competiton.getId()) {
            competiton.setCreateDate(new Date());
            AssertUtil.isTrue(competitonDao.save(competiton) < 1, CeuConstant.OPS_FAILED_MSG);
        } else {
            AssertUtil.isTrue(null == competitonDao.queryById(competiton.getId()), "待更新记录不存在或已删除!");
            AssertUtil.isTrue(competitonDao.update(competiton) < 1, CeuConstant.OPS_FAILED_MSG);
        }

    }

    /**
     * 检查竞赛信息参数
     *
     * @param competitonName
     * @param competitonAuthor
     */
    private void checkCompetitonParams(String competitonName, String competitonAuthor) {
        AssertUtil.isTrue(StringUtils.isBlank(competitonName), "竞赛名称不能为空!");
        AssertUtil.isTrue(StringUtils.isBlank(competitonAuthor), "请提供作者!");
    }

    /**
     * 批量删除竞赛信息
     *
     * @param ids
     */
    public void deleteCompetitonBatch(Integer[] ids) {
        AssertUtil.isTrue(null == ids || ids.length == 0, "请选择待删除记录!");
        AssertUtil.isTrue(competitonDao.deleteBatch(ids) < ids.length, CeuConstant.OPS_FAILED_MSG);
    }
}
