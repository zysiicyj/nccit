package com.nccit.ceu.service;

import com.nccit.base.BaseService;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.dao.PermissionDao;
import com.nccit.ceu.dao.RoleDao;
import com.nccit.ceu.dao.UserRoleDao;
import com.nccit.ceu.po.Permission;
import com.nccit.ceu.po.Role;
import com.nccit.ceu.utils.AssertUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 角色服务
 */
@Service
public class RoleService extends BaseService<Role> {
    @Resource
    private RoleDao roleDao;

    @Resource
    private UserRoleDao userRoleDao;

    @Resource
    private PermissionDao permissionDao;

    public List<Role> queryAllRoles() {
        return roleDao.queryAllRoles();
    }

    /**
     * 1.参数非空校验
     * 角色名称非空
     * 2.角色唯一性校验
     * 3.添加|更新
     *
     * @param role
     */
    public void saveOrUpdateRole(Role role) {
        AssertUtil.isTrue(StringUtils.isBlank(role.getRoleName()), "角色名称不能为空!");
        Role temp = roleDao.queryRoleByRoleName(role.getRoleName());
        role.setUpdateDate(new Date());
        if (null != role.getId()) {
            AssertUtil.isTrue(null != temp && !(temp.getId().equals(role.getId())), "角色名称不能重复!");
            AssertUtil.isTrue(roleDao.update(role) < 1, CeuConstant.OPS_FAILED_MSG);
        } else {
            AssertUtil.isTrue(null != temp, "角色已存在!");
            role.setCreateDate(new Date());
            role.setIsValid(1);
            AssertUtil.isTrue(roleDao.save(role) < 1, CeuConstant.OPS_FAILED_MSG);
        }
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public void deleteRolesBatch(Integer[] ids) {
        AssertUtil.isTrue(null == ids || ids.length == 0, "请选择待删除角色记录!");
        for (Integer roleId : ids) {
            Integer total = userRoleDao.queryUserRoleTotalByRoleId(roleId);
            if (total > 0) {
                AssertUtil.isTrue(userRoleDao.deleteUserRoleByRoleId(roleId) < total, CeuConstant.OPS_FAILED_MSG);
            }
        }
        AssertUtil.isTrue(roleDao.deleteBatch(ids) < ids.length, CeuConstant.OPS_FAILED_MSG);

    }

    /**
     * 执行授权
     *
     * @param rid       角色id
     * @param moduleIds 模块id
     */
    public void addGrant(Integer rid, Integer[] moduleIds) {
        AssertUtil.isTrue(null == rid || rid == 0 || null == roleDao.queryById(rid), "请选择角色记录!");
        /**
         * moduleIds==null   清空角色权限
         */
        /**
         * 第一次授权
         *    直接添加即可
         * 第二次授权
         *    先执行删除 删除角色原始权限 再添加最新的权限
         */
        Integer total = permissionDao.queryPermissionTotalByRoleId(rid);
        if (total > 0) {
            AssertUtil.isTrue(permissionDao.deletePermissionByRoleId(rid) < total, CeuConstant.OPS_FAILED_MSG);
        }
        if (ArrayUtils.isNotEmpty(moduleIds)) {
            /**
             * 批量添加资源到权限表
             */
            List<Permission> permissions = new ArrayList<Permission>();
            for (Integer moduleId : moduleIds) {
                Permission permission = new Permission();
                permission.setCreateDate(new Date());
                permission.setModuleId(moduleId);
                permission.setRoleId(rid);
                permission.setUpdateDate(new Date());
                permissions.add(permission);
            }
            AssertUtil.isTrue(permissionDao.saveBatch(permissions) < moduleIds.length, CeuConstant.OPS_FAILED_MSG);
        }

    }

    /**
     * 根据ID查询权限
     *
     * @param roleIds
     * @return
     */
    public List<String> queryAllPermissionsByRoleIds(List<Integer> roleIds) {
        AssertUtil.isTrue(null == roleIds || roleIds.size() == 0, "角色不存在!");
        return roleDao.queryAllPermissionsByRoleIds(roleIds);
    }
}
