package com.nccit.ceu.service;

import com.nccit.base.BaseService;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.dao.CustomerDao;
import com.nccit.ceu.po.Customer;
import com.nccit.ceu.utils.AssertUtil;
import com.nccit.ceu.utils.MathUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 用户服务
 */
@Service
public class CustomerService extends BaseService<Customer> {

    @Resource
    private CustomerDao customerDao;

    public void saveOrUpdateCustomer(Customer customer) {
        /**
         * 1.参数校验
         *   name 非空  唯一性
         *   phone 非空
         *   fr 非空
         * 2.执行添加|更新
         *    id 是否空
         */
        AssertUtil.isTrue(StringUtils.isBlank(customer.getName()), "用户名称非空!");
        AssertUtil.isTrue(StringUtils.isBlank(customer.getPhone()), "联系方式非空!");
        Customer temp = customerDao.queryCustomerByCusName(customer.getName());
        customer.setUpdateDate(new Date());
        if (null != customer.getId()) {
            AssertUtil.isTrue(null != temp && !(temp.getId().equals(customer.getId())), "用户已存在!");
            AssertUtil.isTrue(customerDao.update(customer) < 1, CeuConstant.OPS_FAILED_MSG);
        } else {
            AssertUtil.isTrue(null != temp, "用户已存在!");
            customer.setIsValid(1);
            customer.setCreateDate(new Date());
            AssertUtil.isTrue(customerDao.save(customer) < 1, CeuConstant.OPS_FAILED_MSG);
        }
    }

    public void deleteCustomersByIds(Integer[] ids) {
        AssertUtil.isTrue(null == ids || ids.length == 0, "请选择待删除记录!");
        AssertUtil.isTrue(customerDao.deleteBatch(ids) < ids.length, CeuConstant.OPS_FAILED_MSG);
    }
}
