package com.nccit.ceu.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nccit.base.BaseService;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.dao.UserDao;
import com.nccit.ceu.dao.UserRoleDao;
import com.nccit.ceu.dto.UserDto;
import com.nccit.ceu.model.UserModel;
import com.nccit.ceu.po.User;
import com.nccit.ceu.po.UserRole;
import com.nccit.ceu.query.UserQuery;
import com.nccit.ceu.utils.AssertUtil;
import com.nccit.ceu.utils.Md5Util;
import com.nccit.ceu.utils.UserIDBase64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;

/**
 * 用户服务
 */
@Service
public class UserService extends BaseService<User> {

    @Resource
    private UserDao userDao;

    @Resource
    private UserRoleDao userRoleDao;

    @Resource
    private RoleService roleService;

    /**
     * 1.参数校验
     * 2.查询用户记录存在
     * 存在
     * 校验密码(加密)
     * 比对成功  用户登录成功 返回用户信息
     * 比对失败   密码不正确
     * 不存在
     * 用户登录非法
     *
     * @param userName
     * @param userPwd
     * @return
     */
    public UserModel userLoginCheck(String userName, String userPwd) {
        checkUserLoginParams(userName, userPwd);
        User user = userDao.queryUserByUserName(userName);
        AssertUtil.isTrue(null == user, "改用户不存在或已注销!");
        AssertUtil.isTrue(!user.getUserPwd().equals(Md5Util.encode(userPwd)), "密码不正确!");
        return buildUserModelInfo(user);
    }

    /**
     * 创建用户信息模型
     *
     * @param user
     * @return
     */
    private UserModel buildUserModelInfo(User user) {
        UserModel userModel = new UserModel();
        userModel.setUserName(user.getUserName());
        userModel.setTrueName(user.getTrueName());
        userModel.setUserIdStr(UserIDBase64.encoderUserID(user.getId()));
        return userModel;
    }

    /**
     * 参数校验
     *
     * @param userName
     * @param userPwd
     */
    private void checkUserLoginParams(String userName, String userPwd) {
        AssertUtil.isTrue(StringUtils.isBlank(userName), "用户名不能为空!");
        AssertUtil.isTrue(StringUtils.isBlank(userPwd), "密码不能为空!");
    }

    /**
     * 1.参数校验
     * userId  记录必须存在
     * oldPassword 必须与数据库中密码一致
     * newPassword   confirmPassword  非空  必须相同
     * 2.执行更新
     * 新密码 加密（md5）
     * 执行更新 判断结果
     *
     * @param userId
     * @param oldPassword
     * @param newPassword
     * @param confirmPassword
     */
    public void updateUserPasswordByUserId(Integer userId, String oldPassword, String newPassword, String confirmPassword) {
        checkUpdatePasswordParams(userId, oldPassword, newPassword, confirmPassword);
        AssertUtil.isTrue(userDao.updateUserPasswordById(userId, Md5Util.encode(newPassword)) < 1, CeuConstant.OPS_FAILED_MSG);
    }

    /**
     * 参数校验
     *
     * @param userId
     * @param oldPassword
     * @param newPassword
     * @param confirmPassword
     */
    private void checkUpdatePasswordParams(Integer userId, String oldPassword, String newPassword, String confirmPassword) {
        User user = userDao.queryById(userId);
        AssertUtil.isTrue(null == userId || null == user, "更新用户不存在!");
        AssertUtil.isTrue(StringUtils.isBlank(oldPassword), "原始密码非空!");
        AssertUtil.isTrue(!user.getUserPwd().equals(Md5Util.encode(oldPassword)), "原始密码不正确!");
        AssertUtil.isTrue(StringUtils.isBlank(newPassword) || StringUtils.isBlank(confirmPassword), "新密码不能为空!");
        AssertUtil.isTrue(!newPassword.equals(confirmPassword), "新密码不一致!");
    }

    /**
     * 查询真实名称
     *
     * @return
     */
    public List<Map<String, Object>> queryCustomerManagers() {
        return userDao.queryCustomerManagers();
    }

    /**
     * 参数查询
     *
     * @param userQuery
     * @return
     */
    public Map<String, Object> queryUsersByParams(UserQuery userQuery) {
        PageHelper.startPage(userQuery.getPageNum(), userQuery.getPageSize());
        List<UserDto> entities = userDao.queryUsersByParams(userQuery);
        if (!CollectionUtils.isEmpty(entities)) {
            for (UserDto userDto : entities) {
                String roleIdStr = userDto.getRoleIdStr();
                if (!StringUtils.isBlank(roleIdStr)) {
                    String[] temp = roleIdStr.split(",");
                    for (String roleId : temp) {
                        userDto.getRoleIds().add(Integer.parseInt(roleId));
                    }
                }
            }
        }
        PageInfo<UserDto> pageInfo = new PageInfo<UserDto>(entities);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("total", pageInfo.getTotal());
        map.put("rows", pageInfo.getList());
        return map;
    }

    /**
     * 1.参数判断
     * 用户名  密码(统一:123456)  trueName email phone 非空
     * 2.用户记录唯一性校验
     * 用户名不能重复
     * 3.执行添加|更新
     * userId
     * 存在  执行更新
     * 不存在  执行添加
     * 4.角色id 处理(用户角色分配)
     * 执行用户添加操作
     * 批量添加用户角色记录到用户角色表
     * 执行用户更新操作
     * 原始 1 2 3
     * 最新
     * 1 2 3 14
     * 1 3
     * null
     * 1.比对
     * 有可能执行记录添加   也有可能执行删除
     * 2.先执行删除  后执行添加
     *
     * @param userDto
     */
    public void saveOrUpdateUser(UserDto userDto) {
        checkUserParams(userDto.getUserName(), userDto.getTrueName(), userDto.getEmail(), userDto.getPhone());
        User temp = userDao.queryUserByUserName(userDto.getUserName());
        userDto.setUpdateDate(new Date());
        User user = new User();
        Integer key = null;
        if (null != userDto.getId()) {
            AssertUtil.isTrue(null != temp && !(temp.getId().equals(userDto.getId())), "该用户已存在!");
            BeanUtils.copyProperties(userDto, user);
            AssertUtil.isTrue(userDao.update(user) < 1, CeuConstant.OPS_FAILED_MSG);
            key = user.getId();
        } else {
            AssertUtil.isTrue(null != temp, "该用户已存在!");
            userDto.setCreateDate(new Date());
            BeanUtils.copyProperties(userDto, user);
            user.setUserPwd(Md5Util.encode("123456"));//密码统一123456
            AssertUtil.isTrue(userDao.save(user) < 1, CeuConstant.OPS_FAILED_MSG);
            key = user.getId();//获取主键
        }
        relationUserRoleInfo(key, userDto.getRoleIds());
    }

    /**
     * 角色分配实现
     * 1.执行删除(删除用户角色原始记录) 根据用户id 执行删除操作
     * 2.执行批量添加(集合内容存在)
     *
     * @param userId
     * @param roleIds
     */
    private void relationUserRoleInfo(Integer userId, List<Integer> roleIds) {
        Integer total = userRoleDao.queryUserRoleTotalByUserId(userId);
        if (total > 0) {
            AssertUtil.isTrue(userRoleDao.deleteUserRoleByUserId(userId) < total, CeuConstant.OPS_FAILED_MSG);
        }
        List<UserRole> userRoles = null;
        if (!CollectionUtils.isEmpty(roleIds)) {
            userRoles = new ArrayList<UserRole>();
            for (Integer roleId : roleIds) {
                UserRole userRole = new UserRole();
                userRole.setUserId(userId);
                userRole.setRoleId(roleId);
                userRole.setCreateDate(new Date());
                userRole.setUpdateDate(new Date());
                userRoles.add(userRole);
            }
            AssertUtil.isTrue(userRoleDao.saveBatch(userRoles) < roleIds.size(), CeuConstant.OPS_FAILED_MSG);
        }
    }

    /**
     * 参数校验
     *
     * @param userName
     * @param trueName
     * @param email
     * @param phone
     */
    private void checkUserParams(String userName, String trueName, String email, String phone) {
        AssertUtil.isTrue(StringUtils.isBlank(userName), "用户名不能为空!");
        AssertUtil.isTrue(StringUtils.isBlank(trueName), "真实名称不能为空!");
        AssertUtil.isTrue(StringUtils.isBlank(email), "邮箱不能为空!");
        AssertUtil.isTrue(StringUtils.isBlank(phone), "手机号不能为空!");
    }

    /**
     * 1.删除用户记录
     * 2.删除用户角色记录
     * 根据userId
     *
     * @param ids
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteUserBatch(Integer[] ids) {
        AssertUtil.isTrue(null == ids || ids.length == 0, "请选择删除的用户记录!");
        for (Integer userId : ids) {
            Integer total = userRoleDao.queryUserRoleTotalByUserId(userId);
            if (total > 0) {
                AssertUtil.isTrue(userRoleDao.deleteUserRoleByUserId(userId) < total, CeuConstant.OPS_FAILED_MSG);
            }
        }
        int a = 1 / 0;
        AssertUtil.isTrue(userDao.deleteBatch(ids) < ids.length, CeuConstant.OPS_FAILED_MSG);
    }

    /**
     * 用户拥有的所有权限
     *
     * @param userName
     * @return
     */
    public List<String> queryUserHasPermissions(String userName) {
        AssertUtil.isTrue(StringUtils.isBlank(userName), "用户名非空!");
        User user = userDao.queryUserByUserName(userName);
        AssertUtil.isTrue(null == user, "该用户不存在!");
        List<Integer> roleIds = userDao.queryAllRoleIdsByUserId(user.getId());
        return roleService.queryAllPermissionsByRoleIds(roleIds);
    }
}
