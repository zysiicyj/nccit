package com.nccit.ceu.service;

import com.nccit.base.BaseService;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.dao.NewsDevPlanDao;
import com.nccit.ceu.po.NewsDevPlan;
import com.nccit.ceu.po.News;
import com.nccit.ceu.utils.AssertUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 新闻开发服务
 */
@Service
public class NewsDevPlanService extends BaseService<NewsDevPlan> {

    @Resource
    private NewsDevPlanDao newsDevPlanDao;

    @Resource
    private NewsService newsService;

    /**
     * 1.参数校验
     * 计划日期
     * 内容
     * 执行效果
     * 2.根据id 值是否存在判断添加|更新
     * 存在:执行更新
     * 判断记录是否存在  -->执行更新操作
     * 不存在
     * 执行添加
     *
     * @param newsDevPlan
     */
    public void saveOrUpdateNewsDevPlan(NewsDevPlan newsDevPlan) {
        checkNewsDevPlanParams(newsDevPlan.getPlanDate(), newsDevPlan.getPlanItem(), newsDevPlan.getExeAffect());
        newsDevPlan.setUpdateDate(new Date());
        if (null != newsDevPlan.getId()) {
            AssertUtil.isTrue(null == newsDevPlanDao.queryById(newsDevPlan.getId()), "待更新记录不存在或已删除!");
            AssertUtil.isTrue(newsDevPlanDao.update(newsDevPlan) < 1, CeuConstant.OPS_FAILED_MSG);
        } else {
            newsDevPlan.setCreateDate(new Date());
            newsDevPlan.setIsValid(1);
            AssertUtil.isTrue(newsDevPlanDao.save(newsDevPlan) < 1, CeuConstant.OPS_FAILED_MSG);
            News news = newsService.queryById(newsDevPlan.getNewsId());
            if (news.getDevResult().equals(0)) {
                news.setDevResult(1);
                AssertUtil.isTrue(newsService.update(news) < 1, CeuConstant.OPS_FAILED_MSG);
            }
        }
    }

    /**
     * 参数校验
     * 1、计划日期
     * 2、计划内容
     * 3、执行效果
     *
     * @param planDate
     * @param planItem
     * @param exeAffect
     */
    private void checkNewsDevPlanParams(Date planDate, String planItem, String exeAffect) {
        AssertUtil.isTrue(null == planDate, "计划项日期不能为空!");
        AssertUtil.isTrue(StringUtils.isBlank(planItem), "请填写计划项内容!");
        AssertUtil.isTrue(StringUtils.isBlank(exeAffect), "执行效果非空!");
    }

    /**
     * 批量删除开发计划
     *
     * @param ids
     */
    public void deleteNewsDevPlanBatch(Integer[] ids) {
        AssertUtil.isTrue(null == ids || ids.length == 0, "请选择待删除记录!");
        AssertUtil.isTrue(newsDevPlanDao.deleteBatch(ids) < ids.length, CeuConstant.OPS_FAILED_MSG);
    }
}
