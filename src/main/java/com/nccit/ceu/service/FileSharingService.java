package com.nccit.ceu.service;

import com.nccit.base.BaseService;
import com.nccit.ceu.constants.CeuConstant;
import com.nccit.ceu.dao.FileSharingDao;
import com.nccit.ceu.po.FileSharing;
import com.nccit.ceu.po.FileSharing;
import com.nccit.ceu.utils.AssertUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 文件
 */
@Service
public class FileSharingService extends BaseService<FileSharing> {

    @Resource
    private FileSharingDao fileSharingDao;

    /**
     * 1.参数校验
     * 参数非空校验
     * fileSharing_name
     * fileSharing_author
     * link_phone
     * 2.判断添加|更新操作 唯一标准-id值是否存在
     * id 存在:执行更新
     * id 不存在:执行添加
     * 3.判断结果
     *
     * @param fileSharing
     */
    public void saveOrUpdateFileSharing(FileSharing fileSharing) {
        checkFileSharingParams(fileSharing.getName(), fileSharing.getAuthor());
        fileSharing.setUpdateTime(new Date());
        if (null == fileSharing.getId()) {
            fileSharing.setCreateTime(new Date());
            fileSharing.setIllegal(1);
            AssertUtil.isTrue(fileSharingDao.insertSelective(fileSharing) < 1, CeuConstant.OPS_FAILED_MSG);
        } else {
            AssertUtil.isTrue(null == fileSharingDao.selectByPrimaryKey(fileSharing.getId()), "待更新记录不存在或已删除!");
            AssertUtil.isTrue(fileSharingDao.updateByPrimaryKeySelective(fileSharing) < 1, CeuConstant.OPS_FAILED_MSG);
        }

    }

    /**
     * 参数校验
     *
     * @param fileSharingName
     * @param fileSharingAuthor
     */
    private void checkFileSharingParams(String fileSharingName, String fileSharingAuthor) {
        AssertUtil.isTrue(StringUtils.isBlank(fileSharingName), "新闻名称不能为空!");
        AssertUtil.isTrue(StringUtils.isBlank(fileSharingAuthor), "请提供作者!");
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public void deleteFileSharingBatch(Integer[] ids) {
        AssertUtil.isTrue(null == ids || ids.length == 0, "请选择待删除记录!");
        AssertUtil.isTrue(fileSharingDao.deleteByPrimaryKey(ids) < ids.length, CeuConstant.OPS_FAILED_MSG);
    }
}
