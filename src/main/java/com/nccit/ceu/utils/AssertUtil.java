package com.nccit.ceu.utils;


import com.nccit.ceu.exceptions.AuthException;
import com.nccit.ceu.exceptions.ParamsException;
import sun.applet.Main;

/**
 * 断言
 */
public class AssertUtil {

    public static void isTrue(Boolean flag, String msg) {
        if (flag) {
            throw new ParamsException(msg);
        }
    }

    public static void isTrue(Boolean flag, String msg, Integer code) {
        if (flag) {
            throw new ParamsException(code, msg);
        }
    }

    public static void isNotLogin(Boolean flag, String msg) {
        if (flag) {
            throw new AuthException(msg);
        }
    }
}
