$(function () {
    $("#dg").datagrid({
        rowStyler: function (index, row) {
            var devResult = row.devResult;
            if (devResult == 0) {
                return "background-color:pink";
            }
            if (devResult == 1) {
                return "background-color:yellow;opacity:0.8";
            }
            if (devResult == 2) {
                return "background-color:Aqua;opacity:0.8";
            }
            if (devResult == 3) {
                return "background-color:red;opacity:0.8";
            }
        }
    })


    $("#dlg").dialog({
        // 监听事件  监听对话框关闭
        onClose: function () {
            initFormData();
        }
    })
});

/**
 * 执行多条件查询
 */
function queryCompetitionByParams() {
    var coName = $("#coName2").val();
    var coAddress = $("#coAddress2").val();
    var time = $("#time").datebox("getValue");

    $("#dg").datagrid("load", {
        coName:coName,
        coAddress:coAddress,
        time:time
    })
}

function openAddCompetitionDialog() {
    //$("#dlg").dialog("open").dialog("setTitle","添加新闻");
    openAddOrUpdateDlg("dlg", "添加竞赛信息");
}

function initFormData() {
    /* $("#fm").form("clear");*/
    $("#createMan").val("");
    $("#coAddress").val("");
    $("#coName").val("");
    $("#coIntroduce").val("");
    $("#id").val("");
}

function closeDlg() {
    //$("#dlg").dialog("close");
    closeDlgData("dlg");
}

function saveOrUpdateCompetition() {
    saveOrUpdateData("fm", ctx + "/competition/saveOrUpdateCompetition", "dlg", queryCompetitionByParams);
}

/**
 * 打开修改对话框
 */
function openModifyCompetitionDialog() {
    openModifyDialog("dg", "fm", "dlg", "更新新闻");
}

function deleteCompetitonDialog() {
    deleteData("dg", ctx + "/competition/deleteCompetitionBatch", queryCompetitionByParams);
}
