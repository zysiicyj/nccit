/**
 * 1、url拼接
 * 2、格式开发状态
 */
$(function () {
    $("#dg").edatagrid({
        url: ctx + "/newsDevPlan/queryNewsDevPlansBySid?sid=" + $("#newsId").val(),
        saveUrl: ctx + "/newsDevPlan/saveOrUpdateNewsDevPlan?sid=" + $("#newsId").val(),
        updateUrl: ctx + "/newsDevPlan/saveOrUpdateNewsDevPlan?sid=" + $("#newsId").val()
    });

    var devResult = $("#devResult").val();
    if (devResult == 2 || devResult == 3) {
        $("#dg").edatagrid("disableEditing");
        $("#toolbar").hide();
    }

});

/**
 *创建新行
 */
function addRow() {
    $("#dg").edatagrid("addRow");
}

/**
 * 保存、更新数据
 */
function saveOrUpdateNewsDevPlan() {
    $("#dg").edatagrid("saveRow");
    $("#dg").edatagrid("load");
}

/**
 * 取消新行
 */
function cancelRow() {
    $("#dg").edatagrid("cancelRow");
}

/**
 * 删除开发计划
 */
function delNewsDevPlan() {
    var rows = $("#dg").edatagrid("getSelections");
    if (rows.length == 0) {
        $.messager.alert("来自ceu", "请选择待删除记录!", "error");
        return;
    }
    $.messager.confirm("来自ceu", "确定执行删除操作?", function (r) {
        if (r) {
            var ids = "ids=";
            for (var i = 0; i < rows.length; i++) {
                if (i <= rows.length - 2) {
                    ids = ids + rows[i].id + "&ids=";
                } else {
                    ids = ids + rows[i].id;
                }
            }
            $.ajax({
                type: "post",
                url: ctx + "/newsDevPlan/delNewsDevPlan",
                data: ids,
                dataType: "json",
                success: function (data) {
                    if (data.code == 200) {
                        $("#dg").edatagrid("load");
                    } else {
                        $.messager.alert("来自ceu", data.msg, "error");
                    }
                }
            })
        }
    })
}

/**
 * 更新开发结果
 * @param devResult
 */
function updateNewsDevResult(devResult) {
    $.ajax({
        type: "post",
        url: ctx + "/news/updateNewsDevResultBySid",
        data: {
            sid: $("#newsId").val(),
            devResult: devResult
        },
        dataType: "json",
        success: function (data) {
            if (data.code == 200) {
                $("#toolbar").hide();
                $("#dg").edatagrid("disableEditing");
            } else {
                $.messager.alert("来自ceu", data.msg, "error");
            }
        }

    })
}