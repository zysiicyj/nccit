$(function () {
    $("#dg").datagrid({
        rowStyler: function (index, row) {
            var auditing = row.auditing;
            if (auditing == 0) {
                return "background-color:pink";
            }
            if (auditing == 1) {
                return "background-color:yellow;opacity:0.8";
            }
        }
    })


    $("#dlg").dialog({
        // 监听事件  监听对话框关闭
        onClose: function () {
            initFormData();
        }
    })
});

/**
 * 文件类型
 * @param val
 * @param row
 * @param index
 * @returns {string}
 */
function formateFileType(val, row, index) {
    if (val == 0) {
        return '音乐';
    }
    if (val == 1) {
        return "电影";
    }
    if (val == 2) {
        return "三级";
    }
}

/**
 * 超链接
 * @param val
 * @param row
 * @param index
 * @returns {string}
 */
function formateLink(val, row, index) {
    return "<a href= http://" + val + ">" + val + "</a>";
}

/**
 * 执行多条件查询
 */
function queryFileSharingByParams() {
    var name = $("#name").val();
    var type = $("#type").val();
    var author = $("#author").val();
    var time = $("#time").datebox("getValue");

    $("#dg").datagrid("load", {
        name: name,
        type: type,
        author: author,
        time: time
    })
}

/**
 *
 * @param val  单元格value
 * @param row  单元格所在行 行记录
 * @param index  单元格所在行 索引
 */
function formateIllegal(val, row, index) {
    if (val == 0) {
        return "违法";
    }
    if (val == 1) {
        return "未违法";
    }
}

/**
 *
 * @param val  单元格value
 * @param row  单元格所在行 行记录
 * @param index  单元格所在行 索引
 */
function formateAuditing(val, row, index) {
    if (val == 0) {
        return "审核未通过";
    }
    if (val == 1) {
        return "审核通过";
    }
}

function initFormData() {
    /* $("#fm").form("clear");*/
    $("#name").val("");
    $("#type").val("");
    $("#author").val("");
    $("#auditing").combobox("setValue", "");
    $("#illegal").combobox("setValue", "");
    $("#id").val("");
}

function closeDlg() {
    //$("#dlg").dialog("close");
    closeDlgData("dlg");
}

function saveOrUpdateFileSharing() {
    saveOrUpdateData("fm", ctx + "/fileSharing/saveOrUpdateFileSharing", "dlg", queryFileSharingByParams);
}

/**
 * 打开修改对话框
 */
function openModifyFileSharingDialog() {
    openModifyDialog("dg", "fm", "dlg", "审核文件");
}

function deleteFileSharingDialog() {
    deleteData("dg", ctx + "/fileSharing/deleteFileSharingBatch", queryFileSharingByParams);
}
