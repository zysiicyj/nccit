/**
 * 根据开发结果更新背景颜色
 */
$(function () {
    $("#dg").datagrid({
        rowStyler: function (index, row) {
            var devResult = row.devResult;
            if (devResult == 0) {
                return "background-color:pink";
            }
            if (devResult == 1) {
                return "background-color:yellow;opacity:0.8";
            }
            if (devResult == 2) {
                return "background-color:Aqua;opacity:0.8";
            }
            if (devResult == 3) {
                return "background-color:red;opacity:0.8";
            }
        }
    })

    $("#dlg").dialog({
        // 监听事件  监听对话框关闭
        onClose: function () {
            initFormData();
        }
    })
});

/**
 * 执行多条件查询
 */
function queryNewsByParams() {
    var newsName = $("#newsName").val();
    var state = $("#state").combobox("getValue");
    var devResult = $("#devResult").combobox("getValue");
    var time = $("#time").datebox("getValue");

    $("#dg").datagrid("load", {
        newsName: newsName,
        state: state,
        devResult: devResult,
        time: time
    })
}

/**
 *格式化发布状态
 * @param val  单元格value
 * @param row  单元格所在行 行记录
 * @param index  单元格所在行 索引
 */
function formateState(val, row, index) {
    if (val == 0) {
        return "未发布";
    }
    if (val == 1) {
        return "已发布";
    }
}

/**
 * 格式化开发结果
 * @param val
 * @returns {string}
 */
function formateDevResult(val) {
    if (val == 0 || val == "") {
        return "未提交";
    }
    if (val == 1) {
        return "审核中";
    }
    if (val == 2) {
        return "审核通过"
    }
    if (val == 3) {
        return "审核失败";
    }
}

/**
 * 打开新闻对话框
 */
function openAddNewsDialog() {
    openAddOrUpdateDlg("dlg", "添加新闻");
}

/**
 * 初始化对话框内容
 */
function initFormData() {
    /* $("#fm").form("clear");*/
    $("#newsAuthor").val("");
    $("#newsName1").val("");
    $("#newsSource").val("");
    $("#newsContent").val("");
    $("#newsTitle").val("");
    $("#assignMan").combobox("setValue", "");
    $("#id").val("");
}

/**
 * 关闭对话框
 */
function closeDlg() {
    closeDlgData("dlg");
}

/**
 * 添加或者更新新闻
 */
function saveOrUpdateNews() {
    saveOrUpdateData("fm", ctx + "/news/saveOrUpdateNews", "dlg", queryNewsByParams);
}

/**
 * 打开修改对话框
 */
function openModifyNewsDialog() {
    openModifyDialog("dg", "fm", "dlg", "更新新闻");
    var rows = $("#dg").datagrid("getSelections");
    $("#assignMan").combobox("setValue", rows[0].trueName);
}

/**
 * 批量删除新闻
 */
function deleteNewsDialog() {
    deleteData("dg", ctx + "/news/deleteNewsBatch", queryNewsByParams);
}
