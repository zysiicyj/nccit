/**
 * 初始化：设置背景颜色
 */
$(function () {
    $("#dg").datagrid({
        rowStyler: function (index, row) {
            var devResult = row.devResult;
            if (devResult == 0) {
                return "background-color:pink";
            }
            if (devResult == 1) {
                return "background-color:yellow;opacity:0.8";
            }
            if (devResult == 2) {
                return "background-color:Aqua;opacity:0.8";
            }
            if (devResult == 3) {
                return "background-color:red;opacity:0.8";
            }
        }
    })
});

/**
 * 参数查询
 */
function queryNewsByParams() {
    var newsName = $("#newsName").val();
    var devResult = $("#devResult").combobox("getValue");
    var time = $("#time").datebox("getValue");
    $("#dg").datagrid("load", {
        newsName: newsName,
        devResult: devResult,
        time: time
    })
}

/**
 * 格式化
 * @param val
 * @returns {string}
 */
function formateDevResult(val) {
    if (val == 0 || val == "") {
        return "未提交";
    }
    if (val == 1) {
        return "审核中";
    }
    if (val == 2) {
        return "审核通过"
    }
    if (val == 3) {
        return "审核失败";
    }
}

/**
 * 格式化
 * @param val
 * @param row
 */
function formateOp(val, row) {
    var devResult = row.devResult;
    if (devResult == 0 || devResult == 1) {
        var href = "javascript:openNewsInfoDialog(" + '"开发数据"' + "," + row.id + ")";
        return "<a href='" + href + "'>开发</a>";
    }
    if (devResult == 2 || devResult == 3) {
        var href = "javascript:openNewsInfoDialog(" + '"详情数据"' + "," + row.id + ")";
        return "<a href='" + href + "'>查看详情</a>";
    }
}

/**
 * 打开新的选项卡
 * @param title
 * @param id
 */
function openNewsInfoDialog(title, id) {
    window.parent.openTab(title + "_" + id, ctx + "/newsDevPlan/index?sid=" + id);
}