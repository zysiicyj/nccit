/*
Navicat MySQL Data Transfer

Source Server         : huan
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : ceu

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-02-04 16:03:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_competition
-- ----------------------------
DROP TABLE IF EXISTS `t_competition`;
CREATE TABLE `t_competition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `co_name` varchar(255) DEFAULT NULL,
  `co_address` varchar(255) DEFAULT NULL,
  `co_introduce` varchar(255) DEFAULT NULL,
  `is_valid` int(11) DEFAULT NULL,
  `create_man` varchar(255) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_competition
-- ----------------------------
INSERT INTO `t_competition` VALUES ('1', '11', '11', '11', '0', '11', '2017-01-10', '2018-02-08');
INSERT INTO `t_competition` VALUES ('2', '222', '22', '22', '1', '22', '2018-02-13', '2018-02-13');
INSERT INTO `t_competition` VALUES ('3', '3', '3', '3', '1', '3', '2018-02-03', '2018-02-03');

-- ----------------------------
-- Table structure for t_customer
-- ----------------------------
DROP TABLE IF EXISTS `t_customer`;
CREATE TABLE `t_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `area` varchar(20) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `post_code` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `web_site` varchar(20) DEFAULT NULL,
  `is_valid` int(4) DEFAULT '1',
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_customer
-- ----------------------------
INSERT INTO `t_customer` VALUES ('1', '北京大牛科技', '北京', '北京海淀区双榆树东里15号', '100027', '010-62263393', '010-62263393', 'www.daniu.com', '1', '2016-02-25 11:28:43', '2016-08-24 18:42:19');
INSERT INTO `t_customer` VALUES ('2', '风驰科技', '北京', '321', '21', '321', '321', '321', '1', '2016-01-25 12:15:19', '2016-11-28 11:46:24');
INSERT INTO `t_customer` VALUES ('17', 'test002', '北京', '绿地伯顿', '2342432', '234324324', '2343', '324324', '1', '2016-04-13 10:32:16', '2017-04-13 10:38:08');
INSERT INTO `t_customer` VALUES ('18', 'test003', '上海', '32434', '234324', '324324', '324324', '23434', '1', '2016-04-13 10:39:33', '2017-04-13 10:39:33');
INSERT INTO `t_customer` VALUES ('20', '腾讯', null, null, null, '123233232', null, null, '1', '2017-01-01 10:13:57', '2017-09-18 10:15:56');
INSERT INTO `t_customer` VALUES ('21', '阿里巴巴', '北京', '浙江杭州', '324324', '23424324324', '2343', 'www.alibaba.com', '1', '2016-01-01 11:12:16', '2017-09-18 11:25:25');
INSERT INTO `t_customer` VALUES ('22', '中国工商银行', '上海', '浦东', '201600', '18920156732', '12312321', 'www.icbc.com', '1', '2016-10-21 10:55:09', '2017-10-21 11:00:02');

-- ----------------------------
-- Table structure for t_file_sharing
-- ----------------------------
DROP TABLE IF EXISTS `t_file_sharing`;
CREATE TABLE `t_file_sharing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` int(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `auditing` int(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `illegal` int(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_file_sharing
-- ----------------------------
INSERT INTO `t_file_sharing` VALUES ('1', '超人', '1', 'www.baidu.com', '1', '王杰', '1', '3', '2018-02-01', '2018-02-03');
INSERT INTO `t_file_sharing` VALUES ('2', '色即是空', '2', 'www.baidu.com', '0', '徐宝', '1', '2', '2018-01-10', '2018-02-14');

-- ----------------------------
-- Table structure for t_news
-- ----------------------------
DROP TABLE IF EXISTS `t_news`;
CREATE TABLE `t_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_source` varchar(255) DEFAULT NULL,
  `news_name` varchar(255) DEFAULT NULL,
  `news_title` varchar(255) DEFAULT NULL,
  `news_author` varchar(255) DEFAULT NULL,
  `news_content` varchar(255) DEFAULT NULL,
  `create_man` varchar(255) DEFAULT NULL,
  `assign_man` varchar(255) DEFAULT NULL,
  `assign_time` datetime DEFAULT NULL,
  `state` int(255) DEFAULT NULL,
  `dev_result` int(255) DEFAULT NULL,
  `is_valid` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_news
-- ----------------------------
INSERT INTO `t_news` VALUES ('1', '11111', '1111', '1111', '1111', '111', 'admin', '42', '2018-02-03 14:51:34', '1', '3', '1', '2018-02-06 17:42:50', '2018-02-03 14:51:34');
INSERT INTO `t_news` VALUES ('2', '12222', '2222', '2222', '2222', '222', '11', '222', '2018-02-21 17:43:24', '111', '2222', '0', '2018-02-14 17:43:42', '2018-02-20 17:43:46');
INSERT INTO `t_news` VALUES ('3', '22222', '222222', '22222', '22222', '22222', 'admin', '老裴', '2018-02-03 14:49:29', '1', '0', '0', '2018-02-03 14:19:02', '2018-02-03 14:49:29');
INSERT INTO `t_news` VALUES ('6', '4', '5', '5', '5', '5', 'admin', '42', '2018-02-03 14:55:39', '1', '1', '1', '2018-02-03 14:20:57', '2018-02-03 14:55:39');
INSERT INTO `t_news` VALUES ('7', '5', '6', '6', '6', '6', 'admin', '老裴', '2018-02-03 14:59:10', '1', '3', '0', '2018-02-03 14:34:43', '2018-02-03 14:59:10');
INSERT INTO `t_news` VALUES ('8', '6', '6', '6', '6', '6', 'admin', '48', '2018-02-03 14:45:00', '1', '2', '1', '2018-02-03 14:45:00', '2018-02-03 14:45:00');
INSERT INTO `t_news` VALUES ('9', '7', '8', '9', '10', '11', 'admin', '42', '2018-02-03 14:52:04', '1', '1', '1', '2018-02-03 14:48:20', '2018-02-03 14:52:04');
INSERT INTO `t_news` VALUES ('10', '', '222222', '22222', 'aaaa', '', 'admin', '42', '2018-02-03 14:49:45', '1', '0', '0', '2018-02-03 14:49:45', '2018-02-03 14:49:45');
INSERT INTO `t_news` VALUES ('11', '11', '222222', '22222', '222', 'aaa', 'admin', '42', '2018-02-03 14:51:02', '1', '0', '0', '2018-02-03 14:51:02', '2018-02-03 14:51:02');

-- ----------------------------
-- Table structure for t_news_dev_plan
-- ----------------------------
DROP TABLE IF EXISTS `t_news_dev_plan`;
CREATE TABLE `t_news_dev_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) DEFAULT NULL,
  `plan_item` varchar(255) DEFAULT NULL,
  `plan_date` datetime DEFAULT NULL,
  `exe_affect` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `is_valid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_news_dev_plan
-- ----------------------------
INSERT INTO `t_news_dev_plan` VALUES ('1', '1', '1', '2018-02-07 14:17:18', '1', '2018-02-28 00:00:00', '2018-02-15 00:00:00', '1');
INSERT INTO `t_news_dev_plan` VALUES ('2', '2', '2', '2018-02-06 14:17:36', '2', '2018-01-17 00:00:00', '2018-02-20 00:00:00', '0');
INSERT INTO `t_news_dev_plan` VALUES ('3', '9', '1', '2018-01-31 00:00:00', '1', '2018-02-03 16:00:52', '2018-02-03 16:00:52', '1');
INSERT INTO `t_news_dev_plan` VALUES ('4', '9', '2', '2018-02-14 00:00:00', '2', '2018-02-03 16:03:14', '2018-02-03 16:03:14', '0');
INSERT INTO `t_news_dev_plan` VALUES ('5', '9', '3', '2018-02-06 00:00:00', '3', '2018-02-03 16:05:55', '2018-02-03 16:05:55', '0');
INSERT INTO `t_news_dev_plan` VALUES ('6', '9', '4', '2018-02-14 00:00:00', '4', '2018-02-03 00:00:00', '2018-02-03 16:07:05', '1');
INSERT INTO `t_news_dev_plan` VALUES ('7', '6', '1', '2018-02-07 00:00:00', '1', '2018-02-03 18:27:43', '2018-02-03 18:27:43', '1');
INSERT INTO `t_news_dev_plan` VALUES ('8', '9', '5', '2018-02-28 00:00:00', '5', '2018-02-03 18:29:04', '2018-02-03 18:29:04', '0');
INSERT INTO `t_news_dev_plan` VALUES ('9', '6', '2', '2018-02-06 00:00:00', '2', '2018-02-03 18:29:34', '2018-02-03 18:29:34', '1');

-- ----------------------------
-- Table structure for t_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_permission`;
CREATE TABLE `t_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL COMMENT '角色ID',
  `module_id` int(11) DEFAULT NULL COMMENT '模块ID',
  `acl_value` varchar(255) DEFAULT NULL COMMENT '权限值',
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=587 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_permission
-- ----------------------------
INSERT INTO `t_permission` VALUES ('1', '1', '1', '10', '2018-02-03 22:16:41', '2018-02-03 22:16:44');
INSERT INTO `t_permission` VALUES ('2', '1', '1', '1010', '2018-02-03 22:17:17', '2018-02-03 22:17:18');
INSERT INTO `t_permission` VALUES ('3', '1', '1', '101001', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('4', '1', '1', '101002', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('5', '1', '1', '101003', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('6', '1', '1', '1020', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('7', '1', '1', '20', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('8', '1', '1', '2010', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('9', '1', '1', '201001', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('10', '1', '1', '201002', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('11', '1', '1', '201003', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('12', '1', '1', '30', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('13', '1', '1', '3010', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('14', '1', '1', '301001', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('15', '1', '1', '301002', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('16', '1', '1', '301003', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('17', '1', '1', '40', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('18', '1', '1', '4010', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('19', '1', '1', '401001', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('20', '1', '1', '401002', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('21', '1', '1', '401003', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('22', '1', '1', '50', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('23', '2', '1', '10', '2018-02-03 22:16:41', '2018-02-03 22:16:44');
INSERT INTO `t_permission` VALUES ('24', '2', '1', '1010', '2018-02-03 22:17:17', '2018-02-03 22:17:18');
INSERT INTO `t_permission` VALUES ('25', '2', '1', '101001', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('26', '2', '1', '101002', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('27', '2', '1', '0', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('28', '2', '1', '1020', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('29', '3', '1', '20', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('30', '3', '1', '2010', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('31', '3', '1', '201001', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('32', '3', '1', '201002', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('33', '3', '1', '0', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('34', '4', '1', '30', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('35', '4', '1', '3010', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('36', '4', '1', '301001', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('37', '4', '1', '301002', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('38', '4', '1', '0', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('39', '5', '1', '40', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('40', '5', '1', '4010', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('41', '5', '1', '401001', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('42', '5', '1', '401002', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('43', '5', '1', '0', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('44', '2', '1', '50', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('45', '3', '1', '50', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('46', '4', '1', '50', '2018-02-03 22:17:37', '2018-02-03 22:17:39');
INSERT INTO `t_permission` VALUES ('47', '5', '1', '50', '2018-02-03 22:17:37', '2018-02-03 22:17:39');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL,
  `role_remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `is_valid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '系统管理员', '系统管理员', '2016-12-01 00:00:00', '2017-10-30 22:29:17', '1');
INSERT INTO `t_role` VALUES ('2', '新闻管理员', '新闻管理员', '2016-12-01 00:00:00', '2016-12-01 00:00:00', '1');
INSERT INTO `t_role` VALUES ('3', '客户管理', '客户管理', '2016-12-01 00:00:00', '2016-12-01 00:00:00', '1');
INSERT INTO `t_role` VALUES ('4', '竞赛信息管理', '竞赛信息管理', '2017-06-30 14:50:24', '2017-06-30 14:50:24', '1');
INSERT INTO `t_role` VALUES ('5', '用户文件管理', '用户文件管理', '2017-09-22 09:31:26', '2017-09-22 09:31:26', '1');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) DEFAULT NULL,
  `user_pwd` varchar(100) DEFAULT NULL,
  `true_name` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `is_valid` int(4) DEFAULT '1',
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'huan', '4QrcOUm6Wau+VuBX8g+IPg==', 'admin', '234', '324', '1', '2017-09-09 00:14:53', '2017-09-11 21:10:37');
INSERT INTO `t_user` VALUES ('2', 'xubao', '4QrcOUm6Wau+VuBX8g+IPg==', 'admin', '234', '324', '1', '2017-09-09 00:14:53', '2017-09-11 21:10:37');
INSERT INTO `t_user` VALUES ('3', 'wangjie', '4QrcOUm6Wau+VuBX8g+IPg==', 'admin', '234', '324', '1', '2017-09-09 00:14:53', '2017-09-11 21:10:37');
INSERT INTO `t_user` VALUES ('4', 'juge', '4QrcOUm6Wau+VuBX8g+IPg==', 'admin', '234', '324', '1', '2017-09-09 00:14:53', '2017-09-11 21:10:37');
INSERT INTO `t_user` VALUES ('5', 'sheng', '4QrcOUm6Wau+VuBX8g+IPg==', 'admin', '234', '324', '1', '2017-09-09 00:14:53', '2017-09-11 21:10:37');

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES ('1', '1', '1', '2017-10-23 11:17:41', '2017-10-23 11:17:44');
INSERT INTO `t_user_role` VALUES ('2', '2', '2', '2017-10-24 11:11:58', '2017-10-24 11:11:58');
INSERT INTO `t_user_role` VALUES ('3', '3', '3', '2017-10-24 11:21:11', '2017-10-24 11:21:13');
INSERT INTO `t_user_role` VALUES ('4', '4', '4', '2017-10-24 11:11:58', '2017-10-24 11:11:58');
INSERT INTO `t_user_role` VALUES ('5', '5', '5', '2017-10-24 11:11:58', '2017-10-24 11:11:58');
