<html>
<head>
<#include "common.ftl" >
    <script type="text/javascript" src="${ctx}/js/news.dev.plan.detail.js"></script>
</head>
<body style="margin: 15px">
<div id="p" class="easyui-panel" title="新闻信息" style="width: 650px;height: 210px;padding: 10px" maximizable="true"
     minimizable="true" collapsible="true" closable="true">
    <table cellspacing="8px">
        <input type="hidden" id="newsId" name="newsId" value="${news.id}"/>
        <input type="hidden" id="devResult" value="${news.devResult}"/>
        <tr>
            <td>新闻名称：</td>
            <td><input type="text" id="newsName" name="newsName" readonly="readonly" value="${(news.newsName)!""}"/>
            </td>
            <td></td>
            <td>新闻来源：</td>
            <td><input type="text" id="newsSource" name="newsSource" readonly="readonly"
                       value="${(news.newsSource)!""}"/></td>
        </tr>
        <tr>
            <td>作&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;者：</td>
            <td><input type="text" id="newsAuthor" name="newsAuthor" readonly="readonly"
                       value="${(news.newsAuthor)!""}"/></td>
        </tr>
        <tr>
            <td>新闻标题：</td>
            <td colspan="4"><input type="text" id="newsTitle" name="newsTitle" style="width: 455px"
                                   readonly="readonly" value="${(news.newsTitle)!""}"/></td>
        </tr>
        <tr>
            <td>创&nbsp;&nbsp;建&nbsp;&nbsp;人：</td>
            <td><input type="text" readonly="readonly" id="createMan" name="createMan" value="${(news.createMan)!""}"/>
            </td>
            <td></td>
            <td>创建时间：</td>
            <td><input type="text" id="createTime" name="createDate" readonly="readonly"
                       value="${news.createDate?string("yyyy-MM-dd")}"/></td>
        </tr>
        <tr>
            <td>指&nbsp;&nbsp;派给&nbsp;&nbsp;：</td>
            <td>
                <input type="text" readonly="readonly" id="assignMan" name="assignMan" value="${(news.trueName)!""}"/>
            </td>
            <td></td>
            <td>指派时间：</td>
            <td><input type="text" id="assignTime" name="assignTime" readonly="readonly"
                       value="${news.assignTime?string("yyyy-MM-dd HH:mm:ss")}"/></td>
        </tr>
    </table>
</div>
<br/>

<table id="dg" title="开发计划项" style="width:650px;height: 300px;"
       toolbar="#toolbar" idField="id" pagination="true" rownumbers="true" align="center" singleSelect="true"
       fitColumns="true">
    <thead>
    <tr>
        <th field="id" width="1">编号</th>
        <th field="planDate" name="planDate" width="1"
            editor="{type:'datebox',options:{required:true,editable:false}}">日期
        </th>
        <th field="planItem" width="1" editor="{type:'validatebox',options:{required:true}}">计划内容</th>
        <th field="exeAffect" width="1" editor="{type:'validatebox',options:{required:true}}">执行效果</th>
    </tr>
    </thead>
</table>

<div id="toolbar">
    <a href="javascript:addRow()" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加计划</a>
    <a href="javascript:delNewsDevPlan()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除计划</a>
    <a href="javascript:saveOrUpdateNewsDevPlan()" class="easyui-linkbutton" iconCls="icon-save" plain="true">保存计划</a>
    <a href="javascript:cancelRow()" class="easyui-linkbutton" iconCls="icon-undo" plain="true">撤销行</a>
    <a href="javascript:updateNewsDevResult(2)" class="easyui-linkbutton" iconCls="icon-kfcg" plain="true">审核通过</a>
    <a href="javascript:updateNewsDevResult(3)" class="easyui-linkbutton" iconCls="icon-zzkf" plain="true">终止开发</a>
</div>

</body>