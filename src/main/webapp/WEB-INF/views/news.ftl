<html>
<head>
<#include "common.ftl" >
    <script type="text/javascript" src="${ctx}/js/base.js"></script>
    <script type="text/javascript" src="${ctx}/js/news.js"></script>
</head>
<body style="margin: 1px">
<table id="dg" class="easyui-datagrid"
       pagination="true" rownumbers="true"
       url="${ctx}/news/queryNewsByParams" fit="true" toolbar="#tb" fitColumns="true" pageSize="20">
    <thead>
    <tr>
        <th field="cb" checkbox="true" align="center"></th>
        <th field="id" width="1" align="center">编号</th>
        <th field="newsSource" width="1" align="center">新闻来源</th>
        <th field="newsName" width="1" align="center">新闻名称</th>
        <th field="newsTitle" width="1" align="center">新闻标题</th>
        <th field="newsAuthor" width="1" align="center">作者</th>
        <th field="newsContent" width="1" align="center">新闻内容</th>
        <th field="createMan" width="1" align="center">创建人</th>
        <th field="createDate" width="1" align="center">创建时间</th>
        <th field="trueName" width="1" align="center">指派人</th>
        <th field="assignTime" width="1" align="center">指派时间</th>
        <th field="state" width="1" align="center" formatter="formateState">发布状态</th>
        <th field="devResult" width="1" align="center" formatter="formateDevResult">开发进度</th>
    </tr>
    </thead>
</table>
<div id="tb">
    <#if permissions?seq_contains("101002")>
        <a href="javascript:openAddNewsDialog()" class="easyui-linkbutton" iconCls="icon-save" plain="true">添加</a>
        <a href="javascript:openModifyNewsDialog()" class="easyui-linkbutton" iconCls="icon-edit"
           plain="true">更新</a>
    </#if>
     <#if permissions?seq_contains("101003")>
         <a href="javascript:deleteNewsDialog()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
     </#if>
    <br/>
    新闻名称：<input type="text" id="newsName"/>&nbsp;&nbsp;|&nbsp;&nbsp;
    状态：
    <select class="easyui-combobox" name="state" id="state" panelHeight="auto">
        <option value="">全部</option>
        <option value="0">未发布</option>
        <option value="1">已发布</option>
    </select>&nbsp;&nbsp;|&nbsp;&nbsp;

    开发结果：
    <select class="easyui-combobox" id="devResult" panelHeight="auto">
        <option value="">全部</option>
        <option value="0">未提交</option>
        <option value="1">审核中</option>
        <option value="2">审核通过</option>
        <option value="3">审核失败</option>
    </select>&nbsp;&nbsp;|&nbsp;&nbsp;

    创建时间：<input id="time" type="text" class="easyui-datebox"></input>&nbsp;&nbsp;
    <a href="javascript:queryNewsByParams()" class="easyui-linkbutton" iconCls="icon-search" plain="true">搜索</a>
</div>


<div id="dlg" class="easyui-dialog " title="添加新闻记录" closed="true"
     style="width: 500px;height:350px" buttons="#bt">
    <form id="fm" method="post">
        <table cellspacing="17" align="center">
            <tr>
                <td>新闻来源：</td>
                <td><input type="text" id="newsSource" name="newsSource"/></td>
            </tr>
            <tr>
                <td>新闻名称：</td>
                <td><input type="text" id="newsName1" class="easyui-validatebox" name="newsName" required="required"/>
                </td>
            </tr>
            <tr>
                <td>新闻标题：</td>
                <td><input type="text" id="newsTitle" class="easyui-validatebox" name="newsTitle" required="required"/>
                </td>
            </tr>
            <tr>
                <td>作&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;者：</td>
                <td><input type="text" name="newsAuthor" id="newsAuthor" class="easyui-validatebox"
                           required="required"/></td>
            </tr>
            <tr>
                <td>新闻内容：</td>
                <td><input type="text" id="newsContent" name="newsContent"/></td>
            </tr>
            <tr>
                <td>分&nbsp;&nbsp;配&nbsp;&nbsp;人：</td>
                <td><input class="easyui-combobox" id="assignMan" name="assignMan"
                           valueField="id" textField="trueName"
                           url="${ctx}/user/queryCustomerManagers" panelHeight="auto" editable="false"/></td>
            </tr>
            <input name="id" id="id" type="hidden"/>
        </table>
    </form>
</div>

<div id="bt">
    <a href="javascript:saveOrUpdateNews()" class="easyui-linkbutton" plain="true" iconCls="icon-save">保存</a>
    <a href="javascript:closeDlg()" class="easyui-linkbutton" plain="true" iconCls="icon-cancel">取消</a>
</div>

</body>
</html>
