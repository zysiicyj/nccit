<html>
<head>
    <#include "common.ftl" >
    <title>Ceu | 后台主页</title>
    <script type="text/javascript" src="${ctx}/js/common.js"></script>
    <script type="text/javascript" src="${ctx}/js/main.js"></script>
</head>
<body class="easyui-layout">
<div region="north" style="height: 78px;background-color: #E0ECFF">
    <table style="padding: 5px" width="100%">
        <tr>
            <td width="50%">
                <img alt="logo" src="${ctx}/images/bglogo.png">
            </td>
            <td valign="bottom" align="right" width="50%">
                <font size="3">&nbsp;&nbsp;<strong>欢迎：</strong>${(user.userName)!"sxt"}</font>【${(user.trueName)!"sxt" }
                】
            </td>
        </tr>
    </table>
</div>
<div region="center">
    <div class="easyui-tabs" fit="true" border="false" id="tabs">
        <div title="首页" data-options="iconCls:'icon-home'">
            <div align="center" style="padding-top: 100px"><font color="blue" size="10">欢迎使用Ceu后台管理系统</font></div>
        </div>
    </div>
</div>
<div region="west" style="width: 200px" title="导航菜单" split="true">
    <div class="easyui-accordion" data-options="fit:true,border:false">
        <#if permissions??>
            <#if permissions?seq_contains("10")>
                <div title="新闻管理" data-options="selected:true,iconCls:'icon-yxgl'" style="padding: 10px">
                   <#if permissions?seq_contains("1010")>
                       <a href="javascript:openTab('新闻发布管理','news/index/0','icon-yxjhgl')" class="easyui-linkbutton"
                          data-options="plain:true,iconCls:'icon-yxjhgl'" style="width: 150px">新闻发布管理</a>
                   </#if>
                   <#if permissions?seq_contains("1020")>
                       <a href="javascript:openTab('新闻开发计划','news/index/1','icon-khkfjh')" class="easyui-linkbutton"
                          data-options="plain:true,iconCls:'icon-khkfjh'" style="width: 150px">新闻开发计划</a>
                   </#if>
                </div>
            </#if>
            <#if permissions?seq_contains("20")>
                <div title="用户管理" data-options="iconCls:'icon-khgl'" style="padding:10px;">
                <#if permissions?seq_contains("2010")>
                    <a href="javascript:openTab('用户信息管理','customer/index','icon-khxxgl')" class="easyui-linkbutton"
                       data-options="plain:true,iconCls:'icon-khxxgl'" style="width: 150px;">用户信息管理</a>
                </#if>
                </div>
            </#if>
           <#if permissions?seq_contains("30")>
               <div title="竞赛信息管理" data-options="iconCls:'icon-jcsjgl'" style="padding:10px">
                     <#if permissions?seq_contains("3010")>
                         <a href="javascript:openTab('赛事管理','competition/index/0','icon-sjzdgl')"
                            class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-sjzdgl'"
                            style="width: 150px;">竞赛信息管理</a>
                     </#if>
               </div>
           </#if>
             <#if permissions?seq_contains("40")>
               <div title="用户文件管理" data-options="iconCls:'icon-jcsjgl'" style="padding:10px">
                     <#if permissions?seq_contains("4010")>
                         <a href="javascript:openTab('文件审核','fileSharing/index/0','icon-sjzdgl')"
                            class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-sjzdgl'"
                            style="width: 150px;">用户文件审核</a>
                     </#if>
               </div>
             </#if>
            <#if permissions?seq_contains("50")>
                <div title="系统管理" data-options="iconCls:'icon-item'" style="padding:10px">
                    <a href="javascript:openPasswordModifyDialog()" class="easyui-linkbutton"
                       data-options="plain:true,iconCls:'icon-modifyPassword'" style="width: 150px;">修改密码</a>
                    <a href="javascript:logout()" class="easyui-linkbutton"
                       data-options="plain:true,iconCls:'icon-exit'" style="width: 150px;">安全退出</a>
                </div>
            </#if>
        </#if>
    </div>
</div>
<div region="south" style="height:55px;background-color: #E0ECFF" align="center">
    版本所有 上海尚学堂 <a href="http://www.nccit.com" target="_blank">www.nccit.com</a>(2016-2026)<br/>
    上海尚学堂地址：上海市松江区荣乐东路2369弄45号绿地伯顿大厦2层 咨询电话：021-67690939<br/>
    上海尚学堂智能科技有限公司 的icp备案号 （沪ICP备16053543号）
</div>

<div id="dlg" class="easyui-dialog" style="width:400px;height:250px;padding: 10px 20px"
     closed="true" buttons="#dlg-buttons">

    <form id="fm" method="post">
        <table cellspacing="8px">
            <tr>
                <td>用户名：</td>
                <td><input type="text" id="userName" name="userName" readonly="readonly" value="${(user.userName)! }"
                           style="width: 200px"/></td>
            </tr>
            <tr>
                <td>原密码：</td>
                <td><input type="password" id="oldPassword" name="oldPassword" class="easyui-validatebox"
                           required="true" style="width: 200px"/></td>
            </tr>
            <tr>
                <td>新密码：</td>
                <td><input type="password" id="newPassword" name="newPassword" class="easyui-validatebox"
                           required="true" style="width: 200px"/></td>
            </tr>
            <tr>
                <td>确认新密码：</td>
                <td><input type="password" id="newPassword2" name="confirmPassword" class="easyui-validatebox"
                           required="true" style="width: 200px"/></td>
            </tr>
        </table>
    </form>
</div>

<div id="dlg-buttons">
    <a href="javascript:modifyPassword()" class="easyui-linkbutton" iconCls="icon-ok">修改</a>
    <a href="javascript:closePasswordModifyDialog()" class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
</div>

</body>
</html>