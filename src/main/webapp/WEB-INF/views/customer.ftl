<html>
<head>
<#include "common.ftl" >
    <script type="text/javascript" src="${ctx}/js/base.js"></script>
    <script type="text/javascript" src="${ctx}/js/customer.js"></script>
</head>
<body style="margin: 1px">
<table id="dg" class="easyui-datagrid"
       pagination="true" rownumbers="true"
       url="${ctx}/customer/queryCustomersByParams" fit="true" toolbar="#tb" fitColumns="true">
    <thead>
    <tr>
        <th field="cb" checkbox="true" align="center"></th>
        <th field="id" width="1" align="center" hidden="true">编号</th>
        <th field="name" width="1" align="center">用户名称</th>
        <th field="phone" width="1" align="center">联系电话</th>
        <th field="area" width="1" align="center">用户地区</th>
        <th field="address" width="2" align="center">用户地址</th>
        <th field="postCode" width="1" align="center">邮政编码</th>
        <th field="fax" width="1" align="center">传真</th>
        <th field="webSite" width="1" align="center">网址</th>
    </tr>
    </thead>
</table>
<div id="tb">
    <#if permissions?seq_contains("201002")>
        <a href="javascript:openModifyCustomerDialog()"
           class="easyui-linkbutton" iconCls="icon-edit" plain="true">修改</a>
    </#if>
    <#if permissions?seq_contains("201003")>
        <a href="javascript:deleteCustomer()" class="easyui-linkbutton"
           iconCls="icon-remove" plain="true">删除</a>
    </#if>
    <br/>
    用户名称:<input type="text" id="cusName"/>
    <a href="javascript:queryCustomersByParams()" class="easyui-linkbutton" iconCls="icon-search" plain="true">搜索</a>
</div>
<div id="dlg" class="easyui-dialog"
     style="width:700px;height:450px;padding: 10px 20px"
     closed="true" buttons="#dlg-buttons">
    <form id="fm" method="post">
        <input type="hidden" id="id" name="id"/>
        <table cellspacing="8px">
            <tr>
                <td>用户名称：</td>
                <td><input type="text" id="name" name="name"
                           class="easyui-validatebox" required="true"/>&nbsp;<font
                        color="red">*</font></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>地区</td>
                <td>
                    <select class="easyui-combobox" style="width: 154px"
                            id="area" name="area" editable="false" panelHeight="auto">
                        <option value="">请选择地区...</option>
                        <option value="北京">北京</option>
                        <option value="南京">南京</option>
                        <option value="上海">上海</option>
                        <option value="广州">广州</option>
                        <option value="天津">天津</option>
                    </select>&nbsp;<font color="red">*</font>
                </td>
            </tr>
            <tr>
                <td>邮政编码：</td>
                <td><input type="text" id="postCode" name="postCode"
                           class="easyui-validatebox" required="true"/>&nbsp;<font
                        color="red">*</font></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>联系电话：</td>
                <td><input type="text" id="phone" name="phone"
                           class="easyui-validatebox" required="true"/>&nbsp;<font
                        color="red">*</font></td>
            </tr>
            <tr>
                <td>传真：</td>
                <td><input type="text" id="fax" name="fax"
                           class="easyui-validatebox" required="true"/>&nbsp;<font
                        color="red">*</font></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>网址：</td>
                <td><input type="text" id="webSite" name="webSite"
                           class="easyui-validatebox" required="true"/>&nbsp;<font
                        color="red">*</font></td>
            </tr>
            <tr>
                <td>用户地址</td>
                <td colspan="4">
                    <input type="text" id="address" name="address"
                           style="width: 400px" class="easyui-validatebox"
                           required="true"/>&nbsp;<font color="red">*</font>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <a href="javascript:saveOrUpdateCustomer()"
       class="easyui-linkbutton" iconCls="icon-ok">保存</a>
    <a href="javascript:closeCustomerDialog()"
       class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
</div>


</body>
</html>
