<html>
<head>
<#include "common.ftl" >
    <script type="text/javascript" src="${ctx}/js/news.dev.plan.js"></script>
</head>
<body style="margin： 1px">
<table id="dg" class="easyui-datagrid"
       pagination="true" rownumbers="true"
       url="${ctx}/news/queryNewsByParams?state=1" fit="true" toolbar="#tb" singleSelect="true" fitColumns="true">
    <thead>
    <tr>
        <th field="id" width="1" align="center">编号</th>
        <th field="newsSource" width="1" align="center">新闻来源</th>
        <th field="newsName" width="1" align="center">新闻名称</th>
        <th field="newsTitle" width="2" align="center">新闻标题</th>
        <th field="newsAuthor" width="1" align="center">作者</th>
        <th field="trueName" width="1" align="center">指派人</th>
        <th field="assignTime" width="1" align="center">指派时间</th>
        <th field="devResult" width="1" align="center" formatter="formateDevResult">开发进度</th>
        <th field="op" width="1" align="center" formatter="formateOp">操作</th>
    </tr>
    </thead>
</table>
<div id="tb">
    新闻名称：<input type="text" id="newsName"/>&nbsp;&nbsp;|&nbsp;&nbsp;
    开发结果：
    <select class="easyui-combobox" id="devResult" panelHeight="auto">
        <option value="">全部</option>
        <option value="0">未提交</option>
        <option value="1">审核中</option>
        <option value="2">审核通过</option>
        <option value="3">审核失败</option>
    </select>&nbsp;&nbsp;|&nbsp;&nbsp;
    创建时间：<input id="time" type="text" class="easyui-datebox"></input>&nbsp;&nbsp;|&nbsp;&nbsp;
    <a href="javascript:queryNewsByParams()" class="easyui-linkbutton" iconCls="icon-search" plain="true">搜索</a>
</div>

</body>
</html>
