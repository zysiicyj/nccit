<html>
<head>
<#include "common.ftl" >
    <script type="text/javascript" src="${ctx}/js/base.js"></script>
    <script type="text/javascript" src="${ctx}/js/competition.js"></script>
</head>
<body style="margin: 1px">
<table id="dg" class="easyui-datagrid"
       pagination="true" rownumbers="true"
       url="${ctx}/competition/queryCompetitionByParams" fit="true" toolbar="#tb" fitColumns="true" pageSize="20">
    <thead>
    <tr>
        <th field="cb" checkbox="true" align="center"></th>
        <th field="id" width="1" align="center">编号</th>
        <th field="coName" width="1" align="center">竞赛名称</th>
        <th field="coAddress" width="1" align="center">竞赛官网</th>
        <th field="coIntroduce" width="1" align="center">竞赛内容</th>
        <th field="createMan" width="1" align="center">创建人</th>
        <th field="createDate" width="1" align="center">创建时间</th>
        <th field="updateDate" width="1" align="center">更新时间</th>
    </tr>
    </thead>
</table>
<div id="tb">
    <#if permissions?seq_contains("301002")>
        <a href="javascript:openAddCompetitionDialog()" class="easyui-linkbutton" iconCls="icon-save"
           plain="true">添加</a>
        <a href="javascript:openModifyCompetitionDialog()" class="easyui-linkbutton" iconCls="icon-edit"
           plain="true">更新</a>
    </#if>
     <#if permissions?seq_contains("301003")>
         <a href="javascript:deleteCompetitonDialog()" class="easyui-linkbutton" iconCls="icon-remove"
            plain="true">删除</a>
     </#if>
    <br/>
    竞赛名称：<input type="text" id="coName2"/>&nbsp;&nbsp;|&nbsp;&nbsp;
    竞赛官网：<input type="text" id="coAddress2"/>&nbsp;&nbsp;|&nbsp;&nbsp;
    创建时间：<input id="time" type="text" class="easyui-datebox"></input>&nbsp;&nbsp;
    <a href="javascript:queryCompetitionByParams()" class="easyui-linkbutton" iconCls="icon-search" plain="true">搜索</a>
</div>


<div id="dlg" class="easyui-dialog " title="添加竞赛信息" closed="true"
     style="width: 500px;height:300px" buttons="#bt">
    <form id="fm" method="post">
        <table cellspacing="17" align="center">
            <tr>
                <td>竞赛名称：</td>
                <td><input type="text" id="coName" class="easyui-validatebox" name="coName" required="required"/></td>
            </tr>
            <tr>
                <td>竞赛地址：</td>
                <td><input type="text" id="coAddress" class="easyui-validatebox" name="coAddress" required="required"/>
                </td>
            </tr>
            <tr>
                <td>作&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;者：</td>
                <td>
                    <input type="text" name="createMan" id="createMan" class="easyui-validatebox" required="required"/>
                </td>
            </tr>
            <tr>
                <td>竞赛内容：</td>
                <td><input type="text" id="coIntroduce" class="easyui-validatebox" name="coIntroduce"
                           required="required"/></td>
            </tr>
            <input name="id" id="id" type="hidden"/>
        </table>
    </form>
</div>

<div id="bt">
    <a href="javascript:saveOrUpdateCompetition()" class="easyui-linkbutton" plain="true"
       iconCls="icon-save">保存</a>
    <a href="javascript:closeDlg()" class="easyui-linkbutton" plain="true" iconCls="icon-cancel">取消</a>
</div>
</body>
</html>
